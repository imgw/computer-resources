# Data Assimilation Research Testbed

A complete documentation can be found here: [https://docs.dart.ucar.edu](https://docs.dart.ucar.edu).


## Compiling on jet
(as of 2024-10-12)

Go into the `DART/build_templates` folder.
Copy a template file, e.g. `mkmf.template.rttov.gfortran` to a file named `mkmf.template`.

Load modules.
```sh
module purge
module load gcc-stack/12.2.0 rttov/v13.2-gcc-12.2.0 
```

Set variables within `mkmf.template`:
```sh
HDF5 = /jetfs/spack/opt/spack/linux-rocky8-skylake_avx512/gcc-12.2.0/hdf5-1.10.10-cnot2wbq45guuozot6n4j3fjbg6tmwyy
RTTOV = /jetfs/manual/rttov/v13.2-gcc-12.2.0/
NETCDF = /jetfs/spack/opt/spack/linux-rocky8-skylake_avx512/gcc-12.2.0/netcdf-fortran-4.6.1-eicuxhegbfmjwqfnu35pqyvlpxoqaowr
```

Go to folder `DART/models/wrf/work`.
Execute `./quickbuild`.
The build process was successful when programs like `filter` were built and can be seen in this folder.

