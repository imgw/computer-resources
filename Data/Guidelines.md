# Shared Data Guideline

    @author: mblaschek
    @email: michael.blaschek@univie.ac.at
    @phone: 53715
    @date: Fri Sep 25 09:15:10 CEST 2020


Data currently shared on the jet cluster:
`PATH /jetfs/shared-data`

Steps:

1. **Be careful.** Do not remove data, unless you are really certain, that this is ok for everyone.
2. Create directories with clear names. **Add permission for others**
3. Ask for help if needed.
4. Add information on your data in the README.md

Here data can be share with other users. 
If you need to modify these data, copy them to scratch and remove later, in order to save storage and duplicates.

## Notice Permission

Typical Permissions are:

    folders: 755   rwxr-xr-x  allows all users to read
    files:   644   rw-r--r--  allows all users to read

Change permission for all directories under that DIR:

`find [DIR] -type d -exec chmod 755 {} \;`

Change permission for all files:

`find [DIR] -type f -exec chmod 644 {} \;`
