# Data Descriptions

Purpose: list available data at the Department of Meteorology and Geophysics.

Edit this file here or on [gitlab](https://gitlab.phaidra.org/imgw/computer-resources/-/blob/master/Data/README.md)

Fill into the appropriate table and add a README to the directory/dataset as well. Use the Data-template.md for example.


| Name | Time period | Resolution | Domain | Variables | Vertical Resolution | Location | Contact | Comments | Source |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| CERA 20C     | 01-1900 to 12-2010 | 2.0 deg x 2.0 deg; 3-hourly   | Global | --- |  91 layers | /jetfs/shared-data/ECMWF/CERA_glob_2deg_3h    | Flexpart group | the data is in Flexpart format! | extracted from ECMWF via flex_extract |
| ERA-Interium | 01-1999 to 01-2019 | 1.0 deg x 1.0 deg; 3-hourly   | Global | --- |  60 layers | /jetfs/shared-data/ECMWF/EI_glob_1deg_3h      | Flexpart group | the data is in Flexpart format! | extracted from ECMWF via flex_extract |
| OPERATIONAL  | 01-2016 to 06-2020 | 1.0 deg x 1.0 deg; 3-hourly   | Global | --- | 137 layers | /jetfs/shared-data/ECMWF/OPER_glob_1deg_3h    | Flexpart group | the data is in Flexpart format! | extracted from ECMWF via flex_extract |
| ERA5         | 01-1959 to 10-2022 | 0.5 deg x 0.5 deg; 1-hourly   | Global | --- | 137 layers | /jetfs/shared-data/ECMWF/ERA5_glob_0.5deg_1h  | Flexpart group | the data is in Flexpart format! | extracted from ECMWF via flex_extract |
| ERA5 europe  | 01-2000 to 10-2022 | 0.25 deg x 0.25 deg; 1-hourly | Europe | --- | 137 layers | /jetfs/shared-data/ECMWF/ERA5_euro_0.25deg_1h | Flexpart group | the data is in Flexpart format! | extracted from ECMWF via flex_extract |
| ERA5 EMME | 01-2020 to 12-2022 | 0.25 deg x 0.25 deg; 1-hourly | EMME region | --- | 137 layers | /jetfs/shared-data/ECMWF/ERA5_EMME_0.25deg_1h | Flexpart group | the data is in Flexpart format! | extracted from ECMWF via flex_extract |

# Observations

| Name | Time period | Temporal resolution | Horizontal resolution | Vertical Resolution | Variables | Location | Contact | Comments | Source |
| --- | --- | --- | ---| --- | --- | --- | --- | --- | --- |


# Satellite




# Model Simulations

ICON-INPUTDATA
 - input data for icon common release 2.6.2.2 installed on jet, contact: climate modeling group of Aiko Voigt

MPI-ESM-INPUTDATA
 - input data for mpiesm-1.2.01p5 installed on jet, contact: climate modeling group of Aiko Voigt
