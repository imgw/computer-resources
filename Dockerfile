FROM python:3.10-buster
ADD requirements.txt /requirements.txt
RUN apt-get update -y \
 && apt-get install -y -qq graphviz sshpass openssh-client rsync \
 && pip install -r requirements.txt
WORKDIR /app
ADD . /app
EXPOSE 8000
CMD ["mkdocs", "serve", "-a", "0.0.0.0:8000"]