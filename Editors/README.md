# Recommendations

Here one can find some help on getting started with some editors or useful configurations or packages to use.

If you have a nice addition please submit it (create an issue).

Thanks. :elephant:


## Micro
![](../mkdocs/img/micro-logo.svg){: width="200px"}

a modern and intuitive terminal-based text editor. [Keyboard shortcuts](https://github.com/zyedidia/micro/blob/master/runtime/help/defaultkeys.md)

Install: [Download](https://micro-editor.github.io)

```sh
# download and install on your local computer
curl https://getmic.ro | bash
# execute
./micro

# or load the module on any IMG server
module load micro
```


## Vim 
![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Vimlogo.svg/64px-Vimlogo.svg.png){: width="100px"}

Vim (a contraction of Vi IMproved) is a clone, with additions, of Bill Joy's vi text editor program for Unix.

This is a very basic editor, in terms of user interface. Functionality is huge. Make yourself comfortable with the [keyboard shortcuts](VIM-Reference.pdf) otherwise you will be stuck. This editor or its relative `vi` is present on any unix system (or linux).

Install: e.g Ubuntu (Debian) [VIM](https://wiki.ubuntuusers.de/VIM/)
Alternative: [VIM](https://www.vim.org/)

Some useful VIM modifications in `~/.vimrc` :

```vim title="Configuration file"
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

 " Make sure we can use backspace delete
noremap! <C-?> <C-h>

" These lines setup the environment to show graphics and colors correctly.
set laststatus=2
set t_Co=256
```

on SRVX2 and JET you can use `:term` inside vim to split the view into a terminal and an editor. Switch between windows with `CTRL+w`. 

Another super useful command is `vimdiff [file1] [file2]` to compare two files and show differences between these two. Switch between sides with `CTRL+w` and use the following shortcuts to:
```
]c :        - next difference
[c :        - previous difference
do          - diff obtain
dp          - diff put
zo          - open folded text
zc          - close folded text
:diffupdate - re-scan the files for differences
```

## Emacs
![](https://www.gnu.org/software/emacs/images/emacs.png){: width="100px"}

This is another major editor that is available on all unix (linux) machines as well. It has a commandline interface and a gui. It is much easier than VIM, but again has a lot of [shortcuts](Emacs-Reference.pdf) to learn.

Install: [Emacs](https://www.gnu.org/software/emacs/)

There are tons of options and customizations in the `~/.emacs` file, which is here [](_emacs)
There are much more things that can be added to `~/.emacs` if you want. Have a look at [CategoryDotEmacs](https://www.emacswiki.org/emacs/CategoryDotEmacs).

## IDEs

### Atom
![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Atom_icon.svg/150px-Atom_icon.svg.png){: width="100px"}

Atom is a free and open-source text and source code editor for macOS, Linux, and Microsoft Windows with support for plug-ins written in JavaScript, and embedded Git Control, developed by GitHub. Atom is a desktop application built using web technologies.

Install: [Atom](https://atom.io/)

This editor can do everything, as it is open source and solutions on common problems exist on github. However, it is for more advanced users, as sometimes you need to change things and fix small bugs. 

### VS-Code
![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/100px-Visual_Studio_Code_1.35_icon.svg.png){: width="100px"}

Visual Studio Code is a freeware source-code editor made by Microsoft for Windows, Linux and macOS. Features include support for debugging, syntax highlighting, intelligent code completion, snippets, code refactoring, and embedded Git. Microsoft has released Visual Studio Code's source code on the `microsoft/vscode` (Code - OSS) repository of GitHub, under the permissive MIT License, while the releases by Microsoft are freeware.

Install: [VS-Code (Microsoft)](https://code.visualstudio.com/)
Open-Source: [OSS](https://github.com/microsoft/vscode)

This is a great editor and has some nice features for Python, however PyCharm is much better for Python. A great feature is remote development. Using VS-code with remote development on SRVX1 or SRVX8 can be done using [instructions](vscode.md) and using a [singularity](https://sylabs.io/guides/3.7/user-guide/) container.

### Pycharm
![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/PyCharm_Icon.svg/150px-PyCharm_Icon.svg.png){: width="100px"}

PyCharm is an integrated development environment (IDE) used in computer programming, specifically for the Python language. It is developed by the Czech company JetBrains. It provides code analysis, a graphical debugger, an integrated unit tester, integration with version control systems (VCSes), and supports web development with Django as well as data science with Anaconda.

PyCharm is cross-platform, with Windows, macOS and Linux versions. The Community Edition is released under the Apache License, and there is also Professional Edition with extra features – released under a proprietary license.

Install: [Pycharm Community](https://www.jetbrains.com/de-de/pycharm/)

This is perfect for python development. However, remote development is not supported by the community version and running it via VNC works.

## Meld
![](https://media-cdn.ubuntu-de.org/wiki/attachments/19/03/meld_logo.png){: width="100px"}

Download: [Meld](https://meldmerge.org/)

Meld is a visual diff and merge tool targeted at developers. Meld helps you compare files, directories, and version controlled projects. It provides **two- and three-way** comparison of both files and directories, and has support for many popular version control systems.

## Joplin
![](https://upload.wikimedia.org/wikipedia/en/thumb/0/08/Joplin-icon.svg/240px-Joplin-icon.svg.png){: width="100px"}

Download: [Joplin](https://joplinapp.org)

Joplin is a free, open source note taking and to-do application, which can handle a large number of notes organised into notebooks. The notes are searchable, can be copied, tagged and modified either from the applications directly or from your own text editor. The notes are in Markdown format.

It can be synced with your u:cloud account using webdav with `Joplin` as the directory in your u:cloud and this as url `https://ucloud.univie.ac.at/remote.php/webdav/Joplin`, using your u:account credentials.
