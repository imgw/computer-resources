# Vim

Is a great editor in the console
As a Gui version there is also `gvim`

Check out the handy References:
- [VIM Keyboard DE](VIM-Keyboard.pdf)
- [VIM Reference](VIM-Reference.pdf)
- [VIMDIFF Reference](VIMDIFF-Reference.pdf)

# Configs
```vim
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
```

# Powerline
Need to install powerline with pip

Config:
```vim
" Allow Powerline style
set rtp+=.local/lib/python3.7/site-packages/powerline/bindings/vim/
" These lines setup the environment to show graphics and colors correctly.
set laststatus=2
set t_Co=256

```

# YAML
Maybe use this config:
```vim
" Allow YAML tab styling
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
let g:indentLine_char_list = ['|', '¦', '?', '?']    
```

# Wired backspace?

in vim [backspace] or backwards delete leaves a wired symbol `^?` and does not delete anything?
Add this to your `~/.vimrc`:
```vim
" Allow backspace
noremap! <C-?> <C-h>
```
The source of this error relates to stty and maybe VNC. 
[on stackoverflow](https://stackoverflow.com/questions/9701366/vim-backspace-leaves)

or maybe
```bash
# in .bashrc
# fix for vim backspace
stty erase '^?'
```

# VIM Diff
Visually compare two files, highlighting the differences. You can easily switch between files (`CTRL+w`) and move parts from one file to another (`do` do obtain or `dp` do put).

```bash
vimdiff file1 file2
```

[VIMDIFF Reference](VIMDIFF-Reference.pdf)