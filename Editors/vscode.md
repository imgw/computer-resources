# VS Code from Microsoft

[Download](https://code.visualstudio.com/)

*Visual Studio Code is a free source-code editor made by Microsoft for Windows, Linux and macOS. Features include support for debugging, syntax highlighting, intelligent code completion, snippets, code refactoring, and embedded Git. Users can change the theme, keyboard shortcuts, preferences, and install extensions that add additional functionality.* (Wikipedia)


## Remote SSH
There is package called `Remote Development` in VSCode which includes 

- Remote SSH
- Remote WSL
- Remote Containers
- Remote Explorer

Although all might be useful, we try to make use of the first one Remote-SSH. This allows to connect with your locally installed VSCode to connect to a remote environment, offering a shell and access to the file system on that host.

![](../mkdocs/vids/vscode-remote-ssh.gif)

Information on:

- [Remote Development with SSH](https://code.visualstudio.com/docs/remote/ssh)
- [Remote Development with SSH and JumpHost](https://support.cs.wwu.edu/home/survival_guide/tools/VSCode_Jump.html)


### Kernel version is too old?

???+ bug "Update 2023"
    Not relevant anymore. All our servers have been upgraded.

What happens if the server has a too old kernel. We are going to use a [singularity container](https://sylabs.io/guides/3.7/user-guide/) to run a more recent version of `node` and allow VSCode to use `srvx8` as well. It will complain a bit, but work.

Here is how:

1. Login to `srvx8`
2. run `/opt/containers/vscode-server/setup.sh`
3. Look inside `$HOME/.vscode-server/singularity.bash` and make sure that path that you require are available in `SINGULARITY_BIND` variable. 
    Your Home directory is always available. Other system path are not.
    - Maybe it is necessary to change the user shell of your account to bash. Run `chsh -s /bin/bash` on remote host (`srvx8`, ...)
4. Switch `remote.SSH.localServerDownload` to `off` in the Remote SSH package.
5. Setup a new host in Remote SSH on your VScode and connect to `srvx8`.

Configure a Python interpreter in VSCode according to the installed module:
 - SRVX8 : `/home/opt/spack/opt/spack/linux-centos6-haswell/gcc-5.3.0/anaconda3-2020.07-opjqtspow2mjqthtdxvx7epz6rntkv2p/bin/python`

#### Updates

Whenever the vscode-server reports an error, it might have happened that it automatically updated and a new version has been installed on the remote host. if so run `~/vscode-server/fix.sh` to make the changes to the run scripts.
Then it should work again.
