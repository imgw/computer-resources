# Compiling & Building

There are a number of tools to build an application. Very often `make` or `cmake` is used to build applications with different complexity.

![](../mkdocs/img/cmake-explanation.png)

To use Make, you have to manually create the Makefile, but with CMake, the Makefile is automatically created.

## Environmental Modules & Makefile

It is quite handy to use environmental modules and load different version of libraries, but how to make use of these ever changing **PATHs**. Take a look at the following examples to help with making your `Makefile` ready for modules.

```makefile
# use the environmental variable $INCLUDE or $CPATH
# split the paths separated by :
INC = $(subst :, ,$(INCLUDE))
INC = $(subst :, ,$(CPATH))
# add a -I/path/to/include
INC := $(INC:%=-I%)
# use the environmental variable $LIBRARY_PATH
LIBS = $(subst :, ,$(LIBRARY_PATH))
LIBS := $(LIBS:%=-L%)
```

With this code snippet in your Makefile you should be able to use environmental variables such as `$INCLUDE`/`$CPATH` or `$LIBRARY_PATH` efficiently. These paths adapt to your loaded modules.

When using `cmake` these paths are found automatically.

important is when writing a makefile is to use tabs

```makefile
target: dependencies
<tab>    command
```

here is an example of a makefile:

```makefile title="Makefile"
triangle: circle.o age.o
    gfortran circle.o triangle.o -o triangle

triangle.o: triangle.f90
    gfortran -c triangle.f90

circle.o: circle.f90
    gfortran -c circle.f90  

clean:
    rm *.o triangle
```

```sh title="running the Makefile"
$ make
gfortran -c circle.f90  
gfortran -c triangle.f90
gfortran circle.o triangle.o -o triangle
$ ./triangle
Enter the radius of the circle:  34
Area of circle with radius   34.00 is      3631.68
```

or writing a cmake `CMakeLists.txt` file

```cmake title="CMakeLists.txt"
cmake_minimum_required(VERSION 3.10.0)

enable_language(Fortran)

project (triangle)

add_executable(triangle triangle.f90 circle.f90)
```

building the application is a bit different

```sh title='running cmake'
$ mkdir build
$ cd build
$ cmake ...
-- The Fortran compiler identification is GNU 13.2.1
-- Detecting Fortran compiler ABI info
-- Detecting Fortran compiler ABI info - done
-- Check for working Fortran compiler: /usr/bin/f95 - skipped
-- The C compiler identification is GNU 13.2.1
-- The CXX compiler identification is GNU 13.2.1
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.6s)
-- Generating done (0.0s)
-- Build files have been written to: ./Fortran/build
$ make
[ 33%] Building Fortran object CMakeFiles/triangle.dir/circle.f90.o
[ 66%] Building Fortran object CMakeFiles/triangle.dir/triangle.f90.o
[100%] Linking Fortran executable triangle
[100%] Built target triangle
$ ./triangle
Enter the radius of the circle:  34
Area of circle with radius   34.00 is      3631.68
```