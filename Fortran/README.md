# Coding

Most often codes are written in Python or Fortran. Here some information is given on Fortran and a little bit of C as well.

Have a look at [Debugging](./Debugging.md)

# Fortran

Fortran is quite popular in Meteorology and Geophysics.
Please find some help on solving common problems.

Get some information on Fortran:
- [Fortran Language, Learning, Compilers](https://fortran-lang.org)

# Compilers

There are a few compilers, but most commonly GNU (Gfortran) and INTEL (ifort) are used on our servers. 

|                                    | GNU Fortran                          | INTEL Fortran                       |
| ---------------------------------- | ------------------------------------ | ----------------------------------- |
| double Fprecision real             | `-fdefault-real-8`                   | `-r8`                               |
| check array bounds                 | `-fbounds-check`                     | `-check`                            |
| call chain traceback               | `-fbacktrace`                        | `-traceback`                        |
| convert little/big endian          | `-fconvert=big-endian/little-endian` | `-convert big_endian/little_endian` |
| default optimisation               | `-O0`                                | `-O2`                               |
| highest recommended optimisation   | `-O3`                                | `-O2` maybe `-O3` or `-fast`        |
| position independent code (shared) | `-fPIC`                              | `-fPIC`                             |


Please take a look into the compiler options guidelines from AMD or Intel, which might be very helpful for finding the right compiler flags for certain CPUs

- Servers such as Aurora [AMD EPYC ZEN3 compiler quick reference](compiler-options-quick-ref-guide-epyc-7xx3-series-processors.pdf)
- Servers such as VSC5, LUMI, ... [AMD EPYC ZEN4 compiler quick reference](compiler-options-quick-ref-guide-amd-epyc-9xx4-series-processors.pdf)
- Servers such as SRVX1, JET, VSC4 [Intel compiler quick reference](quick-reference-guide-intel-compilers-v19-1-final.pdf)

more information on how to find good options can be found on the PRACE (Partnership for advanced computing in Europe) [website](https://prace-ri.eu/training-support/best-practice-guides/)
![](https://prace-ri.eu/wp-content/uploads/logo-main.png)


## Generating optimized code for a certain processor

All compiler providers have special options that have optimizations for certain processors, especially intel.


| Processor Type | System           | Flag (intel)    | Flag (gcc)           |
| -------------- | ---------------- | --------------- | -------------------- |
| Cascade Lake   | VSC5             | `-xCORE-AVX512` | `-march=cascadelake` |
| Skylake        | JET, SRVX1, VSC4 | `-xCORE-AVX512` | `-march=skylake`     |
| Broadwell      | SRVX8            | `-xCORE-AVX2`   | `-march=broadwell`   |
| Sandy Bridge   | DEVx1            | `-xAVX`         | `-march=sandybridge` |
| Milan          | VSC5, Aurora     | `-xCORE-AVX2`   | `-march=znver3`      |
| Generic x64    | containers       | `-xCORE-AVX2`   | `-march=x86-64-v4`   |


[GCC/GFortran processor compiler flags](https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html)

## Intel Compiler

from P. Seibert using ifort for the fastest code (srvx1):
```makefile
# get GRIP_PATH from environment modules
INCPATH = GRIP_API/include
LIBPATH = GRIP_API/lib
# using -xAVX is for old CPUs !!!
FFLAGS = -cpp -xAVX -ipo -O3 -no-prec-div -opt-prefetch  -m64 -mcmodel=medium  -I$(INCPATH)
LDFLAGS = $(FFLAGS) -L$(LIBPATH) -Bstatic -lgrib_api_f90 -lgrib_api -lm -ljasper 
```

Remark: for FLEXPART, otherwise you won't need `grib_api` and `jasper`, or `mcmodel=medium`


## Tricky Issues

### record markers
On 64-bit machines, some Fortran compilers will insert record markers that are 64-bit integers instead of the standard 32-bit integers.

gfortran man page says:
```
-frecord-marker=length
Specify the length of record markers for unformatted files. Valid values for length are 4 and 8. Default is 4. This is different from previous versions of gfortran, which specified a default record marker length of 8 on most systems. If you want to read or write files compatible with earlier versions of gfortran, use -frecord-marker=8.
```


# Code Testing
pFUnit - python Parallel Fortran Unit Testing Framework [GitHub](https://github.com/Goddard-Fortran-Ecosystem/pFUnit)


# Documentation and other resources


Fortran wikibook [DE](https://de.wikibooks.org/wiki/Fortran) [EN](https://en.wikibooks.org/wiki/Fortran)

## Automatic Documentation Generation
FORD: [Overview in Fortran Wiki](http://fortranwiki.org/fortran/show/FORD) | [Code on Github](https://github.com/cmacmackin/ford)
