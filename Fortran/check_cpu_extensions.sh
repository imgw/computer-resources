#!/bin/bash
# By MB
# Check a compiled program and its dependencies on micro architecture extensions
# this can be useful to understand if a certain program will be terminated because it does
# not support the correct architecture. and its libraries too.


install() {
    echo "Downloading elfx86exts tool ..."
    curl -s -L https://github.com/pkgw/elfx86exts/releases/download/elfx86exts%400.6.2/elfx86exts-0.6.2-x86_64-unknown-linux-gnu.tar.gz | tar xvz -C /tmp
}

dependencies(){
    combined=""
    while IFS="\n" read ilib
    do
        # is there a path?
        nobjects=$(echo "$ilib" | wc -w)
        if [ $nobjects -eq 4 ]; then
            # libeccodes.so => /usr/local/lib64/libeccodes.so (0x00007f12b462b000)
            ilibpath=$(echo "$ilib" | cut -d' ' -f3)
            ilibname=$(echo "$ilib" | cut -d' ' -f1)
            if [ -e "$ilibpath" ]; then
                echo "-- dependency: $ilibname"
                ilibextensions=$($CMD "$ilibpath" 2>&1 | grep "Instruction set extensions used:" | sed "s/Instruction set extensions used:/${LIBNAME}/")
                echo "$ilibextensions"
                combined="$combined $ilibextensions"
            fi
        fi
        echo "------------------------------------------------------------"
    done < <(ldd "$1")
    echo "All combined extensions found:"
    ALL_COMBINED=$(echo "$combined" | tr -d , | tr ' ' '\n'| sort -u | tr '\n' ' ')
    echo "$ALL_COMBINED"
    echo
}

CMD=/tmp/elfx86exts
#
# check local executable
#
if [ ! -e "$CMD" ]; then
    install
fi
echo "using: $CMD"

if [ $# -ne 1 ]; then
    cat <<EOF
CPU micro architecture tool

Usage: $0 [EXECUTABLE/LIBRARY]

Please supply an executable or library to be analysed.
EOF
    exit 1
fi

if [ ! -e "$1" ]; then
    echo "Error: $1"
    exit 1
fi

file_type=$(file "$1")

if echo "$file_type"| grep -q executable; then
    echo "Analysing executable: $1"
    echo "$file_type"
    echo -------------------------------------------------------------
    echo Analysing: "$1"
    $CMD "$1"
    echo -------------------------------------------------------------
    echo Analysing: dependencies
    dependencies "$1"
    echo -------------------------------------------------------------
else
    echo "Analysing library: $1"
    echo "$file_type"
    echo -------------------------------------------------------------
    echo Analysing: "$1"
    $CMD "$1"
    echo -------------------------------------------------------------
fi

if command -v lscpu; then
    echo "Your current CPU:"
    lscpu | grep -E 'Model name|Socket|Thread|NUMA|CPU\(s\)' 
    lscpu | grep -E 'Flags' | grep --color -iE "$(echo $ALL_COMBINED | tr ' ' '|')"
    echo -------------------------------------------------------------
fi
