# Getting Started

## Welcome to the Department of Meteorology and Geophysics

Tasks to complete for newcomers, it is recommended that you print this page an tick off your steps:

 - [ ] Request a server account via your supervisor
 - [ ] Receive the inital user account information via mail.
 - [ ] Setup a password manager. [ZID tips](https://zid.univie.ac.at/en/it-worlds/it-security/it-security-tips/password-manager/), e.g. Bitwarden or KeepassXC
 - [ ] How to connect using SSH, read the [connection 101](./SSH-VPN-VNC/README.md)
    - on Windows install a SSH-Client, e.g. Bitwise, MobaXterm, Putty,...
 - [ ] Change your initial password with one of these options:
    - browser ([https://wolke.img.univie.ac.at/ipa/ui](https://wolke.img.univie.ac.at/ipa/ui))
    - terminal `ssh [username]@login.img.univie.ac.at`
 - [ ] Optional: [Create a ssh-key](./SSH-VPN-VNC/Questions.md#q-how-to-use-ssh-key-authentication) and add it to your profile on the [IPA](./SSH-VPN-VNC/IPA.md#add-ssh-key)
 - [ ] Apply for your first [VSC Training course](https://vsc.ac.at/research/vsc-research-center/vsc-school-seminar/), e.g. Introduction to Working on the VSC Clusters
 

## Environment

When you are new to using servers and linux, these few steps might help to get started. However, it is recommended to make a linux introductionary course, e.g. VSC introduction to linux.

Steps:
 
 - [ ] login to aurora using ssh: `ssh [user]@login.img.univie.ac.at` :earth_africa: [More on AURORA](./Servers/Aurora.md)
 - [ ] run: `userpaths` to understand where different data resides. e.g.
    - HOME, SCRATCH (personal), DATA, SHARED, WEBDATA, ?_JET
 - [ ] check available modules by running: `module av` and load anaconda3 module by running: `module load anaconda3`. This should allow you to run some python programs. 
 - [ ] list and unload modules: `module list` and `module purge`. [More on Modules](./Misc/Environment-Modules.md)
 - [ ] run: `userservices` to get some IMGW special tools. Maybe check the weather!?

Please find a useful summary of commands in the [IMGW cheatsheet](./mkdocs/imgw-cheatsheet.pdf)

## Summary of Computing Resources

The Department of Meteorology and Geophysics has access to the following computing resources:

- Teaching Server ([SRVX1](Servers/SRVX1.md))
- Development Server ([Aurora](Servers/Aurora.md))
- Computing Cluster ([JET](Servers/JET.md))
- Vienna Scientific Cluster (requires vsc account, [VSC](VSC.md))

External resources:

- European Center for Medium-Range Weather Forecast (requires additional account, [ECMWF](./ECMWF/README.md))
- Earth Observation Data Center (requires imgw account, [EODC](https://eodc.wolke.img.univie.ac.at), [website](https://eodc.eu))

Please read about access, hardware and quotas at these different resources. A good starting point is [here](./Servers/README.md)