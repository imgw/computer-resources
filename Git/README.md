# Git Tipps and Tools

There are a lot of resources in the web, how to learn git.

Some examples:

- [ZID Introduction to git](https://phaidra.univie.ac.at/detail/o:1403751)
- [Gitlab - Learn Git](https://docs.gitlab.com/ee/tutorials/learn_git.html)
- [Git cheatsheet](./git%20cheat%20sheet.pdf)

Don't be overwhelmed. Everyone learns as they go along, but make sure you have a nice [setup](https://docs.gitlab.com/ee/tutorials/learn_git.html) and understand the [basics](https://docs.gitlab.com/ee/tutorials/learn_git.html).


## HowTo add a specific ssh-key for your git account
Sometimes it might be useful to have different ssh-keys per project or have a special ssh-key just for github/gitlab.

Steps:

1. Generate a `ssh-keygen`
2. Add ssh-key to GitHub or GitLab account under settings
3. Write a `~/.ssh/config` file with a content like this, other examples are in [ECMWF](../ECMWF/README.md).


```ini
Host github.com
	HostName github.com
	User git
	IdentityFile ~/.ssh/id_rsa_for_git

Host gitlab.com
	HostName gitlab.com
	User git
	IdentityFile ~/.ssh/id_rsa_for_gitlab
```

Make sure that you adjust the `Hostname` accordingly and be sure that you use ssh in your git repo:

```bash
$ git remote -v
origin  git@github.com:USER/repo.git (fetch)
origin  git@github.com:USER/repo.git (push)
```

These urls should not show `https`, if there is a `https` then you will need to sign in with your user credentials for gitlab (username and password).

Every git repository can be cloned using https, but for ssh-key access you need the `git@...` version (usually there are two options: `ssh`, and `https`).


## HowTo Sync a GitHub and a GitLab repository
It is easy to import a GitHub repo into GitLab and the otherway around. However, if you want to make sure you can have both repos at the same state, you need to syncronize them.

How to call: `./git-repos-sync [URL1] [URL2] [Branch]`

This means:

- `URL1` - Address of the first remote repository
- `URL2` - Address of the second remote repository
- The order of `URL1` or `URL2` does not matter.
- `Branch` is usually `master`

Different use cases:

1. You have **already** a local copy of either of the repositories (e.g. GitLab or GitHub)
2. You have no local copy of either repository.

??? note "syncronize git repos"
	```bash title="git-repos-sync"
	--8<-- "Git/git-repos-sync"
	```


### Case 1

```bash
cd dir-of-repo
# copy the script there
wget https://gitlab.phaidra.org/imgw/computer-resources/-/raw/master/Git/git-repos-sync
# make executable
chmod +x git-repos-sync
# execute the script
./git-repos-sync [URL] [URL] [Branch]
```

### Case 2

```bash
# Create a new folder to do the sync, can be any name
mkdir sync-repos
# copy the script there
wget https://gitlab.phaidra.org/imgw/computer-resources/-/raw/master/Git/git-repos-sync
# make executable
chmod +x git-repos-sync
# execute the script
./git-repos-sync [URL] [URL] [Branch]
```
