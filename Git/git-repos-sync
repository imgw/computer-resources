#!/bin/bash
# author: https://github.com/adidik/git-repos-sync
# sync two git repositories (github and gitlab)
# modified: 23.4.2021 (MB)
# License: MIT

if [ "$#" -ne 3 ]; then
    >&2 echo "Usage: git-repos-sync <repository URL> <repository URL> <branch-to-sync>"
    exit 1
fi

# Check if you are at root level (fails if not)
if ! git rev-parse --show-toplevel; then
	>&2 echo "Git repo found"
	if [ $PWD == $(git rev-parse --show-toplevel) ]; then
		echo "Not at root of local repo"
		echo $(git rev-parse --show-toplevel)
		exit 1
	fi
fi

if [ ! -d ".git" ]; then
	echo "Git repo for syncronization is not found, creating one..."
	git init
	git fetch $1
	git checkout -b master FETCH_HEAD	
	echo
fi

if ! git diff-index --quiet HEAD --; then
    >&2 echo "Local modifications found, looks like your in the conflict resolution. Resolve a conflict and commit. Then rerun script."
    exit 1
fi

echo "Left: $1"
echo "Right: $2"
echo 
echo "Fetch latest commits from branch $3 in $1"
if ! git fetch -u $1 $3:left/$3; then 
	>&2 echo "Fatal: unable to fetch from $1, rerun the script as soon as connnection restored."
	exit 1
fi 	
echo "Fetch latest commits from branch $3 in $2"
if ! git fetch -u $2 $3:right/$3; then
	>&2 echo "Fatal: unable to fetch from $2, rerun the script as soon as connnection restored."
	exit 1
fi
if git checkout --quiet -b sync-$3 right/$3; then
	echo "Merge branches from left and right if necessary."
	if ! git merge -m "Merge to sync between $1 and $2" --log left/$3; then
                >&2 echo "Merge conflict. Solve it manually, commit and rerun script."
                exit 1
    fi
else
	echo "Rerun after merge conflict resolution or restored connection."
	git checkout --quiet sync-$3
	echo "Try to merge with right first"
	if ! git merge -m "Merge to sync between $1 and $2" --log right/$3; then
                >&2 echo "Merge conflict. Solve it manually, commit and rerun script."
                exit 1
    fi
	if ! git merge -m "Merge to sync between $1 and $2" --log left/$3; then
                >&2 echo "Merge conflict. Solve it manually, commit and rerun script."
                exit 1
    fi
fi

echo "Push merged changes in $3 to $2"
read -p "[USER] continue (y/n)" REPLY
# echo "REPLY: $REPLY"
if [ "$REPLY" == "y" ]; then
	if ! git push $2 HEAD:$3; then
		>&2 echo "Fatal: unable to push to $2, rerun the script as soon as connection restored."
		exit 1
	fi
else
	echo "Abort not pushed to $2"
fi	

echo "Push merged changes in $3 to $1"
read -p "[USER] continue (y/n)" REPLY
# echo "REPLY: $REPLY"
if [ "$REPLY" == "y" ]; then
	if ! git push $1 HEAD:$3; then
		>&2 echo "Fatal: unable to push to $1, rerun the script as soon as connection restored."
		exit 1
	fi
else
	echo "Abort not pushed to $1"
fi	

git checkout --quiet master
git branch -D --quiet sync-$3
echo "Done."
