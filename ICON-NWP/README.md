# ICON-NWP
This is a short introduction to using the ICON-NWP model on `jet` ([ICON-NWP](./ICON-NWP.md)) and how you can [run assimilation](./how-to-bacy.md) experiments with [`bacy`](./bacy.md). 

You can get more information about the model [here](https://code.mpimet.mpg.de/projects/iconpublic).