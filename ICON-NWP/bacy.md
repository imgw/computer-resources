bacy (short for **ba**sic **cy**cling) is a collection of scripts that run assimilation
cycles with ICON. It is modular in structure with each module performing steps
like computing the dynamical model evolution (more), data assimilation (core),
surface, snow and sea surface analysis.


# BACY_1.0

This document gives you a short introduction into the usage of BACY_1.0.
A detailed userguide and further documentation is available at
`git@gitlab.dkrz.de:dace/dace_doc.git` under dace_doc/org/dace_bacy.git
and is continuously being updated.

## How to get BACY_1.0?

BACY_1.0 is under version control using `git`. To obtain BACY_1.0 from the
remote repository `git@gitlab.dkrz.de:dace/dace_bacy.git` the following steps on
your local machine are necessary:
~~~
git clone git@gitlab.dkrz.de:dace/dace_bacy.git <my_bacy_name>
cd <my_bacy_name>/modules
./copy_defaults.sh
~~~
*If you want to get access to the DKRZ-gitlab, someone at DWD or DKRZ needs to approve, maybe someone from the DA or Climate group can help you.*

## How to use BACY_1.0? (Overview)

### Modules

BACY_1.0 pursues a MODULAR CONCEPT. Each MODULE is an self-sufficient unit.
The only contact points to its environment are realized by INTERFACES. 
These are the directories `iodir/input` and `iodir/output` which are
part of each module.

The `prep_<module>` and `save_<module>` scripts take care of PROVIDING the
module with input data from and of STORING its output data to a central place,
respectively. In this way the COMMUNICATION between modules is realized. 
Unless the `input` directory is not filled with input data, the module itself 
cannot be run. Otherwise, the module can start its work and it uses its working
directory `run`.

The behaviour of a module can be controlled via its INPUT ARGUMENTS during the
`<module>` script call and via its PARAMETERS defined in the configuration file
`<module>_conf.sh`. Parameters which are needed by more than one module are
outsourced to the COMMON configuration file `common/bacy_conf.sh`.

For a more detailed module-setup please read `modules/module/README`. This
module is a dummy module and serves as TEMPLATE to help you to create your own
modules. In particular, in its README file you can find some valuable hints
which rely on NAMING CONVENTIONS, classification of variables, etc., which are
IMPORTANT to maintain the modular concept.

In summary: to run a module stand-alone, the following steps are necessary.
~~~
prep_<module> [-h] [-d] arg1 ... argn
<module>      [-h] [-d] arg1 ... argn
save_<module> [-h] [-d] arg1 ... argn
~~~

The option `[-h]` gives you an explanation of the usage of the script and the
option `[-d]` calls the script in a dry-run, meaning that no jobs will be
submitted.

### A cycle

The module `cycle` plays a special role and cannot be understood as a module in
the usual sense (see above). The main difference is the absence of the `input`,
`output` and `run` directories. The purpose of `cycle` is the provision of an
environment in which the order of individual module calls can be controlled.
Thus, this is the central place to arrange all modules to set up an experiment.

In summary: to start an experiment, the following steps are necessary.

    prep_cycle [-h]      arg1 ... argn
    cycle      [-h] [-d] arg1 ... argn

For the options `[-h]` and `[-d]` see the explanations above.

### Default files

The configuration files `<module>_conf.sh` as well as namelists and job
scripts are not under version control. Instead we use default-files, e.g.,
`<module>_conf.sh.default`. This has the advantage that subsequent calls of 
`git pull` will not change the current configuration.

The script `copy_defaults.sh` creates the normal files from the default
versions. This must be done whenever you clone a fresh BACY from the
repository!

## How to make your own developments on BACY_1.0?

The BACY_1.0 MAIN DEVELOPMENT BRANCH is called `bacy-dev`. This branch provides
the BACY_1.0 REFERENCE which has to be the BASIS for ALL users! Only a few
people can push to `bacy-dev` to keep this branch away from accidental or not
quality proven commits.

If you want to contribute to a NEW FEATURE for BACY_1.0, first of all create a
new branch `<project>-<feature>-<team>`, which you can setup as `remote`
to share results inside your team.

During your development it is IMPORTANT to regularly pull from the `bacy-dev`
branch to keep your `basic` BACY_1.0 up to date.

Once you have FINISHED your contribution and you have tested, tested and again
tested your work and you are sure that you match all naming conventions for
BACY_1.0 (see above), then you are ready to contact one person responsible
which merges (together with you) your contribution into `bacy-dev`.
