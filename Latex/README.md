# Latex 

*LaTeX (/ˈlɑːtɛx/ LAH-tekh or /ˈleɪtɛx/ LAY-tekh, often stylized as $`\LaTeX`$) is a software system for **document preparation**. When writing, **the writer uses plain text as opposed to the formatted text** found in "What You See Is What You Get" word processors like Microsoft Word, LibreOffice Writer and Apple Pages. The writer uses **markup tagging conventions to define the general structure of a document** (such as article, book, and letter), to stylise text throughout a document (such as bold and italics), and to add citations and cross-references. **A TeX distribution such as TeX Live** or MiKTeX is used to **produce an output file** (such as **PDF** or DVI) suitable for printing or digital distribution.* @[Wiki](https://en.wikipedia.org/wiki/LaTeX)

What you need to get started:

1. A latex distribution, e.g. texlive or Miktex
    * Or a online latex service, e.g. [Overleaf](https://www.overleaf.de), [latex4technics](https://www.latex4technics.com)
2. An Editor, e.g. TexStudio or Texshop, ...
3. Examples:
    * Open [Seminararbeit.tex ](Seminararbeit.tex) or as [PDF](Seminararbeit.pdf) or online [on Overleaf](https://de.overleaf.com/read/whhnjmbwsvpy)
    * other [Examples](https://www.latextemplates.com)
    * More [Overleaf-Examples](https://de.overleaf.com/latex/examples)

[Advanced Guide to writing scientific papers with $`\LaTeX`$ (Wissenschaftliche Arbeiten schreiben mit $`\LaTeX`$ )](Wissenschaftliches-Script-Latex-Seibert-05.2021.pdf) or the [latest](https://homepage.univie.ac.at/petra.seibert/files/latexscript.pdf)

# Overleaf

Employees and students can use the LaTeX editor Overleaf in their browser to collaborate on LaTeX documents online. Overleaf helps to facilitate collaborative writing, editing and creation of research papers or project reports. [ZID Overleaf](https://zid.univie.ac.at/en/overleaf/)
[Overleaf Limits](https://www.overleaf.com/learn/how-to/Overleaf_plan_limits) and [Timeout Compile Tips](https://www.overleaf.com/learn/how-to/Fixing_and_preventing_compile_timeouts )

Employees have the Overleaf pro version.

# What to use?

**5 Reasons for Latex**

1. Professional PDF with printing quality
2. Lots of mathemetical, physical, chemical or other equations. Need to be in Latex anyway.
3. Need to use a fixed layout given to you by the university or journal
4. Programming like typsetting, with predictable outcome, as compared to other word processors.
5. Readable and shareable

**5 Reasons for Markdown**

1. Markdown is much easier to learn and visual
2. Readable and shareable, easy to collaborate
3. Markdown can be converted into HTML, PDF, DOCX, ...
4. You see what you get editors exist for Markdown with live preview
5. Other content like Maths formulas or tables can be added easily.

**Tip** - If you don not have a template yet and are unsure how to start. Start using Markdown and when needed convert to any Latex template.


# Markdown and Latex

An easier solution to start is to use Markdown, which is a yet another plain text markdown language that supports a lot of nice features:

* latex rendering
* code highlighting
* Gitlab, GitHub integration

It is easy to write, share and version control. Markdown files can be converted to Latex and then compiled to a PDF.

You can find an example ...

What you need to get started to convert to PDF:

1. [Pandoc](https://pandoc.org/installing.html)
2. Latex (Note tips when installing pandoc)
    * fonts-extra
3. Editor for Markdown e.g. [Dillinger (online)](https://dillinger.io) or [Typora](https://typora.io)

At first you can start writing your document just as 
```
pandoc -d eisvogel.tex -o MyPaper.pdf 
```

Links:

- [Jabref - Reference manager (opensource, all platforms)](https://www.jabref.org)
- [Zotero - Reference manager (all platforms)](https://www.zotero.org)
- [Herbert Voß - Latex Referenz der Umgebungen (Makros, Längen, Zähler)](http://www.lehmanns.de/pdf/latexreferenz.pdf)
- [Table Converter Online (csv, xml, json, excel, md, tex, ...)](https://tableconvert.com)
- [Pandoc Demos](https://pandoc.org/demos.html)
- [Academic Pandoc template](https://maehr.github.io/academic-pandoc-template/) ![](https://maehr.github.io/academic-pandoc-template/android-chrome-512x512.png){: width="30px"}
- [Eisvogel Pandoc Template](https://github.com/Wandmalfarbe/pandoc-latex-template) ![](https://github.com/Wandmalfarbe/pandoc-latex-template/blob/master/icon.png?raw=true){: width="30px"}
