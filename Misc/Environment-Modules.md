# Environment modules

![](https://upload.wikimedia.org/wikipedia/en/thumb/0/0a/Environment_Modules_logo.svg/320px-Environment_Modules_logo.svg.png)

Using [environment modules](https://modules.readthedocs.io/en/latest/) it is possible to have different software libraries (versions, compilers) side-by-side and ready to be loaded. Be aware that some libraries are dependent on others. It is recommended to load the highest rank library first to check what dependencies are loaded as well. e.g.:
```bash
$ module load eccodes/2.18.0-intel-20.0.2-6tadpgr 
```
loads the `ECCODES` library and all dependencies. e.g. intel compilers, as indicated by the naming.
```bash
$ module list
Currently Loaded Modulefiles:
 1) zlib/1.2.11-intel-20.0.2-3h374ov     3) hdf5/1.12.0-intel-20.0.2-ezeotzr              5) netcdf-c/4.7.4-intel-20.0.2-337uqtc  
 2) openmpi/4.0.5-intel-20.0.2-4wfaaz4   4) parallel-netcdf/1.12.1-intel-20.0.2-sgz3yqs   6) eccodes/2.18.0-intel-20.0.2-6tadpgr  
```
`module list` shows the currently loaded modules and reports that 6 libraries need to be loaded as dependencies for `ECCODES`. Thus, it is not necessary to load the other libraries manually as they are dependencies of `ECCODES`. However it will be necessary to load the intel compiler suite `intel-parallel-studio/composer.2020.2-intel-20.0.2-zuot22y` as well for build applications.


```bash title="using environment modules"
# unload modules
$ module unload eccodes/2.18.0-intel-20.0.2-6tadpgr 

# unload all modules at once (useful in jobs, before loading the correct ones)
$ module purge

# show information from a module (defined variables)
$ module show eccodes/2.18.0-intel-20.0.2-6tadpgr
-------------------------------------------------------------------
/jetfs/spack/share/spack/modules/linux-rhel8-skylake_avx512/eccodes/2.18.0-intel-20.0.2-6tadpgr:

module-whatis   ecCodes is a package developed by ECMWF for processing meteorological data in GRIB (1/2), BUFR (3/4) and GTS header formats.
conflict        eccodes
prepend-path    PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/bin
prepend-path    LIBRARY_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/lib
prepend-path    LD_LIBRARY_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/lib
prepend-path    C_INCLUDE_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/include
prepend-path    CPLUS_INCLUDE_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/include
prepend-path    INCLUDE /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/include
prepend-path    PKG_CONFIG_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/lib/pkgconfig
prepend-path    CMAKE_PREFIX_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/eccodes-2.18.0-6tadpgreot7jf4yoaiqmqueiihhdcsxk/
-------------------------------------------------------------------
```

## Compilers

### GNU
gfortran is available on the default command search path, so it is not essential to load a module, unless: (1) You need to set environment variables such as FC and F77; (2) You need to link MPI libraries. In these cases, you should load the gfortran module. This is what the module does to the environment:


```sh title="module details gcc"
$ module show gcc/8.3.1-gcc-8.3.1-pp3wjou 
-------------------------------------------------------------------
/jetfs/spack/share/spack/modules/linux-rhel8-skylake_avx512/gcc/8.3.1-gcc-8.3.1-pp3wjou:

module-whatis   The GNU Compiler Collection includes front ends for C, C++, Objective-C, Fortran, Ada, and Go, as well as libraries for these languages.
conflict        gcc
prepend-path    MANPATH /usr/share/man
prepend-path    ACLOCAL_PATH /usr/share/aclocal
prepend-path    PKG_CONFIG_PATH /usr/lib64/pkgconfig
prepend-path    PKG_CONFIG_PATH /usr/share/pkgconfig
setenv          CC      /usr/bin/gcc
setenv          CXX     /usr/bin/g++
setenv          FC      /usr/bin/gfortran
setenv          F77     /usr/bin/gfortran
prepend-path    LD_LIBRARY_PATH /usr/lib64:/usr/lib
prepend-path    PATH /usr/bin
prepend-path    CMAKE_PREFIX_PATH /usr
setenv          F90     /usr/bin/gfortran
-------------------------------------------------------------------
```

### INTEL
there are a few version of intel compilers installed and some more might be added as well:

```bash title="module details intel"
$ module av intel
----------------------- /jetfs/spack/share/spack/modules/linux-rhel8-skylake_avx512 ------------------------
intel-mkl/2020.3.279-gcc-8.3.1-5xeezjw                      
intel-mkl/2020.3.279-intel-20.0.2-m7bxged                   
intel-oneapi-compilers/2021.2.0-oneapi-2021.2.0-6kdzddx     
intel-oneapi-mpi/2021.2.0-oneapi-2021.2.0-haqpxfl           
intel-parallel-studio/composer.2020.2-intel-20.0.2-zuot22y  

--------------------------- /jetfs/spack/share/spack/modules/linux-rhel8-haswell ---------------------------
intel-parallel-studio/composer.2017.7-intel-17.0.7-disfj2g  
```

This shows that we have `intel-parallel-studio` with version `20.0.2` and `17.0.7` installed and `intel-oneapi-compilers` at version `2021.2.0`. The first does not come with Intel-MPI, but the second does.

Again the module sets a lot of 

```bash title="module details intel-parallel-studio"
$ module show intel-parallel-studio/composer.2020.2-intel-20.0.2-zuot22y
-------------------------------------------------------------------
/jetfs/spack/share/spack/modules/linux-rhel8-skylake_avx512/intel-parallel-studio/composer.2020.2-intel-20.0.2-zuot22y:

module-whatis   Intel Parallel Studio.
conflict        intel-parallel-studio
prepend-path    PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/bin
prepend-path    MANPATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/man
prepend-path    LIBRARY_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/lib
prepend-path    LD_LIBRARY_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/lib
prepend-path    C_INCLUDE_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/include
prepend-path    CPLUS_INCLUDE_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/include
prepend-path    INCLUDE /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/include
prepend-path    CMAKE_PREFIX_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/
prepend-path    CLASSPATH ...linux/mpi/intel64/lib/mpi.jar:...linux/daal/lib/daal.jar
prepend-path    CPATH ...linux/ipp/include:...linux/mkl/include:...linux/pstl/include:...linux/pstl/stdlib:...linux/tbb/include:...linux/tbb/include:...linux/daal/include
setenv          DAALROOT        ...linux/daal
prepend-path    FI_PROVIDER_PATH ...linux/mpi/intel64/libfabric/lib/prov
prepend-path    INTEL_LICENSE_FILE /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/licenses:...linux/licenses:/opt/intel/licenses:/home/spack/intel/licenses
setenv          IPPROOT ...linux/ipp
setenv          I_MPI_ROOT      ...linux/mpi
prepend-path    LD_LIBRARY_PATH ...linux/compiler/lib/intel64_lin:...linux/mpi/intel64/libfabric/lib:...linux/mpi/intel64/lib/release:...linux/mpi/intel64/lib:...linux/ipp/lib/intel64:...linux/mkl/lib/intel64_lin:...linux/tbb/lib/intel64/gcc4.8:...linux/daal/lib/intel64_lin:...linux/daal/../tbb/lib/intel64_lin/gcc4.4:...linux/daal/../tbb/lib/intel64_lin/gcc4.8
prepend-path    LIBRARY_PATH ...linux/mpi/intel64/libfabric/lib:...linux/ipp/lib/intel64:...linux/compiler/lib/intel64_lin:...linux/mkl/lib/intel64_lin:...linux/tbb/lib/intel64/gcc4.8:...linux/tbb/lib/intel64/gcc4.8:...linux/daal/lib/intel64_lin:...linux/daal/../tbb/lib/intel64_lin/gcc4.4:...linux/daal/../tbb/lib/intel64_lin/gcc4.8
setenv          MKLROOT ...linux/mkl
prepend-path    NLSPATH ...linux/compiler/lib/intel64/locale/%l_%t/%N:...linux/mkl/lib/intel64_lin/locale/%l_%t/%N
prepend-path    PKG_CONFIG_PATH ...linux/mkl/bin/pkgconfig
setenv          PSTLROOT        ...linux/pstl
setenv          TBBROOT ...linux/tbb
prepend-path    MANPATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/man/common::/opt/xcat/share/man:/opt/xcat/share/man::/opt/slurm/share/man:/opt/slurm/share/man:
prepend-path    PATH ...linux/bin/intel64:...linux/bin:...linux/mpi/intel64/libfabric/bin:...linux/mpi/intel64/bin:/home/spack/.local/bin:/home/spack/bin:/opt/xcat/bin:/opt/xcat/sbin:/opt/xcat/share/xcat/tools:/jetfs/userservices:/jetfs/home/mblaschek/bin:/jetfs/home/mblaschek/.local/bin:/jetfs/spack/bin:/opt/slurm/bin:/usr/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/share/Modules/bin:/jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/parallel_studio_xe_2020.2.108/bin
setenv          CC      /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/bin/icc
setenv          CXX     /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/bin/icpc
setenv          FC      /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/bin/ifort
setenv          F77     /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/bin/ifort
setenv          F90     /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/bin/ifort
setenv          CC      /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/compilers_and_libraries/linux/bin/intel64/icc
setenv          CXX     /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/compilers_and_libraries/linux/bin/intel64/icpc
setenv          FC      /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/compilers_and_libraries/linux/bin/intel64/ifort
setenv          F90     /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/compilers_and_libraries/linux/bin/intel64/ifort
setenv          F77     /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/compilers_and_libraries/linux/bin/intel64/ifort
prepend-path    LD_LIBRARY_PATH /jetfs/spack/opt/spack/linux-rhel8-skylake_avx512/intel-20.0.2/intel-parallel-studio-composer.2020.2-zuot22yfoe7jl67ttimvkzghwluvyaas/lib/intel64
-------------------------------------------------------------------
```

