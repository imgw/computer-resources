# Miscellaneous Topics	

## Customize your shell
On a Linux system there are numerous ways of customizing user experience. And of course there is a way to customize the shell. It should be common by now, that people use [BASH](https://de.wikipedia.org/wiki/Bash_(Shell)), if not run `chsh -s /bin/bash` in your terminal and if you are not sure run `echo $SHELL` wich should show bash.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Gnu-bash-logo.svg/216px-Gnu-bash-logo.svg.png)

The main files to make your shell unique is to edit two files:

- `.bashrc` - Main configuration file for each terminal, as oposed to `.bash_profile` that is only loaded when you first login.
- `.inputrc` - Configuration file for keybaord shortcuts

Please find examples of these two files here [bashrc](./bashrc) [inputrc](./inputrc). Please adjust these files to your needs. Some parts are only available on special systems.

One can also jsut download the user shell initial script:

```sh title="Deploy user shell customizations"
# download and execute
$ wget -O- https://webdata.wolke.img.univie.ac.at/scratch/deploy/setup_user_bash_one.sh | sh
# find two addtions to your .bashrc 

if [ -f $HOME/.imgw_bashrc ]; then
    . $HOME/.imgw_bashrc
fi
if [ -f $HOME/.imgw_alias ]; then
    . $HOME/.imgw_alias
fi
# a new .inputrc
```


## Q: How to extract a table from a PDF?

There is a tool for investigative reporting at news organizations, that developed this tool.  [Go to tabula](https://tabula.technology/)

### How to Use Tabula

1. run tabula depending on your OS and got to http://localhost:8080
2. Upload a PDF file containing a data table.
3. Browse to the page you want, then select the table by clicking and dragging to draw a box around the table.
4. Click "Preview & Export Extracted Data". Tabula will try to extract the data and display a preview. Inspect the data to make sure it looks correct. If data is missing, you can go back to adjust your selection.
5. Click the "Export" button.
6. Now you can work with your data as text file or a spreadsheet rather than a PDF

Note: Tabula only works on text-based PDFs, not scanned documents.

## Q: What is my process doing?

If you experience processes that are slower than they should be or you are unsure what it is doing. There are certain ways to get more information. 

### Process States
For example if you run `ps ux` or `htop` and check your process, you should see a state with the following meaning:
<pre>
    D = UNINTERRUPTABLE_SLEEP
    R = RUNNING & RUNNABLE
    S = INTERRRUPTABLE_SLEEP
    T = STOPPED
    Z = ZOMBIE
</pre>
Processes in a **"D"** or uninterruptible sleep state are usually waiting on I/O. The ps command shows a "D" on processes in an uninterruptible sleep state. The vmstat command also shows the current processes that are "blocked" or waiting on I/O. The vmstat and ps will not agree on the number of processes in a "D" state, so don't be too concerned. You cannot kill "D" state processes, even with `SIGKILL` or `kill -9`. As the name implies, they are uninterruptible. You can only clear them by rebooting the server or waiting for the I/O to respond. It is normal to see processes in a "D" state when the server performs I/O intensive operations.

As root you can do `echo w > /proc/sysrq-trigger` and check the `dmesg -T` what the stacktrace reports.

Processes in a **"S"** or interruptable sleep state are waiting for the user or data to continue. Unlike processes in a **"T"** or stopped state, which must have been suspended, with e.g. Ctrl+Z. Using `jobs` to see what processees you have or running `fg` to get the processes back to your command line.

Processes in a **"Z"** or zombie state are being terminated by the parent processes. If the parent did not clean up, a zombie processes lingers. 

**How to kill a zombie processes?**
 The first step is to find its parent process. Typically there is a column in the "ps" output for PPID or Parent Process ID. You want to identify this number. Once you do you can send a SIGCHLD signal to that parent to tell it to perform this cleanup.

`kill -s SIGCHLD <PPID>`

In worst case scenarios where the parent is generating a lot of zombie processes that are not getting cleaned up, you can kill or restart the parent. 
