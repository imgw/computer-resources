# User Shells

Almost every linux shell has ways to modify the user experience. There are multiple ways of changing your shell environment.

Here are some topics listed:

  - modifying the shell user limits
  - virtual terminals

## Limits

It is possible to address resource limits in a user shell, which make sure that programs do not run out of control. This can be useful for a code that might have an infinite recursion. This is usually controlled by the stack size limit.

Check what resource limits you have:

```sh
$ ulimit -a
core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 3091355
max locked memory       (kbytes, -l) unlimited
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 10000
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited

# in an slurm job it might be necessary to use
ulimit -s unlimited
# to allow a larger stack size for e.g. FORTRAN programs.
```

Stack contains all local variables & data, intermediate storage for registers, and function parameters. This setting is used to block infinite recursion.


| flags | description                                                                                                                                                                                           |
| ----- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| -H    | Specifies that the hard limit for the given resource is set. If you have root user authority, you can increase the hard limit. Anyone can decrease it                                                 |
| -S    | Specifies that the soft limit for the given resource is set. A soft limit can be increased up to the value of the hard limit. If neither the -H nor -S flags are specified, the limit applies to both |
| -a    | Lists all of the current resource limits                                                                                                                                                              |
| -b    | The maximum socket buffer size                                                                                                                                                                        |
| -c    | The maximum size of core files created                                                                                                                                                                |
| -d    | The maximum size of a process's data segment                                                                                                                                                          |
| -e    | The maximum scheduling priority ("nice")                                                                                                                                                              |
| -f    | The maximum size of files written by the shell and its children                                                                                                                                       |
| -i    | The maximum number of pending signals                                                                                                                                                                 |
| -l    | The maximum size that may be locked into memory                                                                                                                                                       |
| -m    | The maximum resident set size (many systems do not honor this limit)                                                                                                                                  |
| -n    | The maximum number of open file descriptors (most systems do not allow this value to be set)                                                                                                          |
| -p    | The pipe size in 512-byte blocks (this may not be set)                                                                                                                                                |
| -q    | The maximum number of bytes in POSIX message queues                                                                                                                                                   |
| -r    | The maximum real-time scheduling priority                                                                                                                                                             |
| -s    | The maximum stack size                                                                                                                                                                                |
| -t    | The maximum amount of cpu time in seconds                                                                                                                                                             |
| -u    | The maximum number of processes available to a single user                                                                                                                                            |
| -v    | The maximum amount of virtual memory available to the shell and, on some systems, to its children                                                                                                     |
| -x    | The maximum number of file locks                                                                                                                                                                      |
| -T    | The maximum number of threads                                                                                                                                                                         |
