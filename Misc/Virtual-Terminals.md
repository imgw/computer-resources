# Virtual Terminals

in Linux it is quite common to have virtual terminals or mulitplexer, which allows to have multiple terminal sessions in one window. This is similar to the `nohup` command which does not send a "hangup" (SIGHUP) signal, therefore continues to run. Running a VNC-Session is similar but is used for graphical applications mainly.

There are especially [**tmux**](https://en.wikipedia.org/wiki/Tmux) and [**screen**](https://en.wikipedia.org/wiki/GNU_Screen) that are widely used on HPC or servers.

![tmux](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Tmux_logo.svg/365px-Tmux_logo.svg.png)

![screen](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Gnuscreen.png/450px-Gnuscreen.png)

Although tmux and screen work similarly, it shall be noted that tmux os prefered.

## TMUX
[Ubuntu Wiki - DE](https://wiki.ubuntuusers.de/tmux/)

There are a lot of specific things that can be done with tmux, but here are some examples on how to use it. Please also refer to the [keybindings](tmux-keyboard-bindings.png).

List all tmux session for your user:
```
tmux ls 
```
Start a new tmux session named imgw:
```
tmux new -s imgw
```
Now you are inside the new tmux session. If you run `exit` you will leave the session and terminate it as well. If you want the session to continue, you have to use `CTRL+b` then push `d` for detach. So the `CTRL+b` puts you into command mode similar to a VI command mode, and then you can issue commands. Please refer to the keybindings to get more information.

Connect to an existing Session named imgw:
```
tmux attach -t imgw
```

Terminate all tmux session for your user:
```
tmux kill-server 
```

A useful feature is to create a split screen in tmux. so inside a tmux session switch to command mode (`CTRL+b`) and push `"` for horizontal split or `%` for vertical split. Moving between splits or here called panes can be done in multiple ways, but easily with `CTRL+b` and the arrow-keys ![arrowkeys](https://media-cdn.ubuntu-de.org/wiki/thumbnails/6/62/62ca4de87dac191968e0278e8a342da9ce26ad59i66x.png)

enable mouse srolling and a longer history, write to `~/.tmux.conf`:
```
set -g mouse on
set -g history-limit 30000
```

### Tmux inside JupyterLab on TeachingHub

By coincidence the Jupyterlab uses the same control sequence to toggle the file browser in jupyterlab as tmux. `CTRL+b` .

To change that, we will just remove the jupyterlab shortcut.
In Jupyterlab go to Settings, Advanced Settings Editor, Keyboard Shortcuts. In the user pane add this:

```yaml
{
    "shortcuts":[
        {
            "command": "application:toggle-left-area",
            "keys": [
                "Accel B"
            ],
            "selector": "body",
            "disabled": true
        }
        ]
}
```

When you can click then save button on the upper right corner, you are fine. Otherwise there might be a syntax error, as indicated in the lower right corner. That might happen if you need to merge things. Now you should be able to control tmux again inside the jupyterlab.


### Sharing a tmux session with other users?
In the first terminal, start tmux where shared is the session name and shareds is the name of the socket: 

```
tmux -S /tmp/shareds new -s shared
```
Then chgrp the socket to a group that both users share in common. In this example, joint is the group that both users share. If there are other users in the group, then they also have access. So it might be recommended that the group have only the two members. 

```
chgrp joint /tmp/shareds
```
In the second terminal attach using that socket and session. 
```
tmux -S /tmp/shareds attach -t shared
```

After finishing sharing it is highly recommanded to remove the device with
```
rm /tmp/shareds
```

This has been used on VSC4, SRVX1, JET.

## SCREEN
[Ubuntu Wiki - DE](https://wiki.ubuntuusers.de/Screen/)

There are a lot of specific things that can be done with screen, but here are some examples on how to use it. Please also refer to the [keybindings](screen-keyboard-bindings.png).

List all screen session for your user:
```
screen -ls 
```
Start a new screen session named imgw:
```
screen -S imgw
```
Now you are inside the new screen session. If you run `exit` you will leave the session and terminate it as well. If you want the session to continue, you have to use `CTRL+a` then push `d` for detach. So the `CTRL+a` puts you into command mode similar to a VI command mode, and then you can issue commands. Please refer to the keybindings to get more information.

Connect to an existing Session named imgw:
```
screen -r imgw
```
Please have a look for more features and use cases e.g. [linuxize](https://linuxize.com/post/how-to-use-linux-screen/)

## Sharing a screen session needs to be allowed by root.
Not available on any IMGW system.
