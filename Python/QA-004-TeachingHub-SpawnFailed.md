# Connecting to TeachingHub

*Reported on 11.12.2020*

There is a problem with previous configurations on the Teaching Hub.

Failure to launch/spawn a notebook/jupyter server is the result of some previous configuration files.

If you experience messages like this:

![Server Spawn](pics/Server_spawn.png)

## Steps

1. Connect to SRVX1 in the terminal `ssh [user]@srvx1.img.univie.ac.at`
2. Remove the `rm -rf .jupyter/` directory 
3. Restart the server on the Teaching Hub
