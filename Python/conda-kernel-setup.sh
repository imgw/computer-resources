#!/bin/bash
# By MB
# Jumpstart a python environment for my students

# Activate anaconda module
module load anaconda3
# Create a new kernel that is based on anaconda, but with some additional packages
# potentially specify which python version, e.g. python=3.8
conda create -y -n ClimatePy3 numpy ipykernel zarr
# This will create a ~/.conda/envs/ClimatePy3/ directory
# using ~/.conda/envs/ClimatePy3/bin/python3 in a script is possible

# activate that environment by sourcing the conda environment
source $(dirname $(which conda))/../etc/profile.d/conda.sh
# and activating your new
conda activate ClimatePy3

# deactivate environment
conda deactivate
# FIN