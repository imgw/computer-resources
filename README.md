![](./mkdocs/img/logo_uniwien.jpg){: width="300px"}
![](./mkdocs/img/logo_img2_color.png){: width="300px"}

# Welcome to the Department of Meteorology and Geophysics

Here you can find help with your computer/server related problems. 
[link to this page on wolke](https://wolke.img.univie.ac.at/documentation/general/index.html)

Search with the top bar or go through the directories:

- [Python related Problems](./Python/README.md)
- [SSH, VNC, VPN related Problems](./SSH-VPN-VNC/README.md)
- [Editors](./Editors/README.md) and [remote connection](./SSH-VPN-VNC/README.md)
- [Data availability and location](./Data/README.md)
- [Git related problems](./Git/README.md) / [Report new issus on gitlab](https://gitlab.phaidra.org/imgw/computer-resources/-/issues)

for new employees or students, you could start with the [Getting Started](./Getting%20Started.md) section or the [Student](./Students/README.md) section.

A useful summary of handy tips is available as [IMGW Cheat sheet (PDF)](./mkdocs/imgw-cheatsheet.pdf)


**If you care to participate please do so:**

- Raise an [Issue on Gitlab](https://gitlab.phaidra.org/imgw/computer-resources/-/issues/?sort=created_date&state=all&first_page_size=20) :earth_africa:
- Give some feedback ([mail](mailto:it.img-wien@univie.ac.at), [mattermost](https://discuss.phaidra.org/imgw/channels/bugs)) :snowman:
- Write to [individual members of the department](https://img.univie.ac.at/en/about-us/staff/). :frog:
