# IPA

*Identity, Policy and Audit* management system for distributed servers and central managed identities. More information [here](https://www.freeipa.org/page/About). This is currently hosted under [wolke](https://wolke.img.univie.ac.at/ipa/ui), which is the web interface and allows easy access for users and managers.

## Access

The IPA has a web interface ([UI](https://wolke.img.univie.ac.at/ipa/ui)) and can only be accessed from UNIVIE Networks. Please use a VPN or connect via [SSH](./SSH.md).

### Password Rules
A new password must use at least 2 of the following classes:

- Upper-case characters
- Lower-case characters
- Digits
- Special characters (for example, punctuation)

**Minimum** length is **8**, password **history** is **4**. The minimum **lifetime** of a password is **1 hour**.

### SSH Login
Please use the given credentials (*username, first-time password*) and login to e.g. *login.img.univie.ac.at*:

![](../mkdocs/img/ipa-ssh-login.png)

You will be asked to change your password immediately.

### Web interface
When inside the UNIVIE network you can access the web interface under [https://wolke.img.univie.ac.at/ipa/ui](https://wolke.img.univie.ac.at/ipa/ui) and login with your credentials. If your password has been reset or the first time you will be asked to change your onetime password immediately.

#### Login
Please use the given credentials (*username, first-time password*) and login to the web interface:

![](../mkdocs/img/ipa-login.png)

You will be asked to change your first-time password immediately. 

#### Add SSH key
Please login to the UI web interface and go to your user page:

![](../mkdocs/img/ipa-add-ssh.png)

Choose *Add* in the SSH public keys section and add your **public** SSH Key here. If you do not know how to create one look into the [SSH](./SSH.md) Section or simply use this command: `ssh-keygen`. It is strongly advised to use a passphrase to secure your key. More information in the SSH Section.

![](../mkdocs/img/ipa-add-ssh-pub.png)
![](../mkdocs/img/ipa-add-ssh-save.png)
Do not forget to save your changes and finally you should see the fingerprint of your key added.
![](../mkdocs/img/ipa-add-ssh-final.png)
Done.
The IPA needs to sync this to all connected servers and this might take up to 5 minutes. Please be patient.
