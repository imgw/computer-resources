# Questions and Answers

Please feel free to add you questions and anwsers.

Links:

- [SSH explained](https://blog.teamairship.com/intro-to-ssh-secure-shell-101-exercise)😂
- [SSH agents explained](https://smallstep.com/blog/ssh-agent-explained/)

## Q: How does ssh work?
Secure Shell (SSH) is a popular networking protocol that lets us access a remote computer over an insecure network such as the Internet.

Secure Shell also supports both password and key-based authentication. Password-based authentication let users provide username and password to authenticate to the remote server. A key-based authentication allows users to authenticate through a key-pair. The key pairs are two cryptographically secure keys for authenticating a client to a Secure Shell server.

![ssh key exchange diagram](../mkdocs/img/ssh-key-exchange.png)

Secure Shell has a client-server architecture. Typically, a server administrator installs a server program that accepts or rejects the incoming connections. Besides, a user runs a client program on their system that requests the server. By default, the server listens on HTTP port 22.

## Q: How to use ssh-key authentication?

In order to connect passwordless to a remote server a secure shell key needs to be generated. This key will be used automatically to login.

???+ danger "SSH KEY Authentication"

    Using this **can be a safer way** to connect to our servers. However, if someone gets access to your key, e.g. on your Laptop, that person has access to your data/server. **Secure your ssh-key.** Therefore consider using a phassphrase / ssh-agent.

```sh title="How to create an RSA key"
# Please use at least 4096 bits for generating your key.
$ ssh-keygen -b 4096 -t rsa
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in .ssh/id_rsa
Your public key has been saved in .ssh/id_rsa.pub
The key fingerprint is:
SHA256:lHEKWnP+1vdNfbSELXApKpFvllcgCp3DHsgrbO2RYuo mblaschek@pop-os
The key's randomart image is:
+---[RSA 4096]----+
|    ..B =..o...  |
|     =.&.=..ooo  |
|  . o +.X o oo o.|
|   * = + B o  o.o|
|  + + . S + . ..+|
| .   .   .   . oo|
|.               o|
| E               |
|                 |
+----[SHA256]-----+
```

```sh title="How to create a secure key"
# this is the newest and securest standard
$ ssh-keygen -t ed25519 -a 100
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/user/.ssh/id_ed25519.
Your public key has been saved in /home/user/.ssh/id_ed25519.pub.
The key fingerprint is:
SHA256:sin8cTe10fOIbSpQW8gETSUKIx27viPolF0w79cBTzU user@srvx1.img.univie.ac.at
The key's randomart image is:
+--[ED25519 256]--+
|    ..+..+o.E    |
|     ..+ .oo .   |
|    o . oo..     |
|     + . ++ ..   |
|      = S.ooo o  |
|   + + +...o = + |
|  o.+ * o.+ o + .|
| .. .o.= ... o   |
| ..  .o.   ..    |
+----[SHA256]-----+
```

It is recommended to use a password to encrpyt the private key `.ssh/id_rsa`. However, this password is then required each time to login. Using an `ssh-agent` can solve that problem.

???+ warning "Adding ssh keys to our servers"
    Remember to use the [IPA for adding your ssh-keys](./IPA.md#add-ssh-key), as keys in `~/.ssh/` on all our servers will be ignored.

## Q: How to use an ssh-agent?

Using an SSH-Agent will make your connection even safer, as your private key is encrypted with a passphrase. First create a new ssh-key and **use a passphrase**. Then continue with using the ssh-agent. This agent is installed on all our servers and it is allowed to forward authentication as you go along.

```bash title="Using an ssh-agent"
# Launch the SSH Agent on your Laptop/Computer
eval `ssh-agent`
# this will add all your ssh-keys from ~/.ssh/
# You will be asked a passphrase if you have one
ssh-add
# or specify the keyfile yourself:
ssh-add path/to/secret/keyfile/secret_key

# The  you need to add your ssh-key.pub (public) to the IPA

# and connect (no password prompt)
ssh [user]@[server]
# Kill the agent and all store secure information
ssh-agent -k
```

???+ warning "ssh-agent on servers"

    Please use these commands on our servers: `ssh-agentstart` and `ssh-agentreconnect`. These commands take care that you do not launch too many agents, killing the system.

[Adding a ssh-key to the IPA](./IPA.md#add-ssh-key).

adding this to your local `.ssh/config` will forward your ssh-agent to be used to hop from server to server.

```sh title="Allow ssh-agent forwarding"
$ vi .ssh/config

Host [host address]
     ForwardAgent yes
```

e.g.

```sh title="ssh-agent hopping"
# check a ssh-agent is running on your Laptop
$ ssh-add -l
256 SHA256:3ppnVv7Cw monkey@laptop (ED25519)
# set the option manually or modify the .ssh/config
$ ssh -oForwardAgent=yes srvx1
# on the sever all your ssh-keys should be available as well
[monkey@srvx1] $ ssh-add -l
256 SHA256:3ppnVv7Cw monkey@laptop (ED25519)
# since this key is registered in the IPA, connecting to other servers is easy
[monkey@srvx1] $ ssh jet02
# with no password prompt using your ssh-agent
[monkey@jet02] $ 
```

[Nice summary of how an ssh-agent works](https://smallstep.com/blog/ssh-agent-explained/)

Keep in mind that you can use the ssh-agent with [KeepassXC](https://keepassxc.org/docs/#faq-ssh-agent-how), find a nice tutorial [here](https://ferrario.me/using-keepassxc-to-manage-ssh-keys/). This is really convenient as it allows you to use all keys in the Keepass as long as it is unlocked. The keys will be automatically removed when the keepass is locked. :)

[![KeepassXC](../mkdocs/img/logo-keepassxc.png){: width="200px"}](https://keepassxc.org/)


## Q: How to transfer files between two VPN networks?

You should be able to use an SSH tunnel via a gateway server

Situation

```
  VPN-1                                              VPN-2
__________    /|       ___________      /|         __________
|  local |   | |       | gateway |     | |         | remote |
|        |---| |----<>------     |   Firewall      |        |
|        | Firewall    |    \---<>-----| |---------|        |
__________   | |       ___________     | |         __________
             |/                        |/
```

Assuming you're trying to transfer a file from/to a remote computer ("remote") from/to your local computer ("local"),
establish a tunnel via a third computer ("gateway") by typing this on your local computer:

```bash
$ ssh -fNL 12345:remote:22 gatewaylogin@gateway
```

Then you can run an unlimited amount of SCP commands on this tunnel (still typing on your local computer):

```bash
# scp [SRC] [DEST]
$ scp -P 12345 remotelogin@localhost://path/to/remote/file /local/path/where/you/want/file
```

another option is to use `rsync` with a gateway command:

```bash
# rsync [SRC] [DEST]
$ rsync -avz -r --stats --progress -e "ssh gateway ssh" remote:/src/documents/ /dest/documents
```

**Note: key-based authentication is required from the gateway to the remote server using rsync.**
Initial testing showed that using key-based authentication between gateway and remote is required.

## Q: How to connect to Jet, Aurora?

Currently there is only `login.img.univie.ac.at` available from the internet.
Please replace `[USER]` with your username on these servers and adjust the servers to your needs.

```bash
$ ssh -t [USER]@login.img.univie.ac.at 'ssh [USER]@jet01.img.univie.ac.at'
```

or using the above `~/.ssh/config` you can do:

```bash
$ ssh -t login ssh jet
```

or using the connect script

```bash
$ connect2jet -g login jet
```

## Q: How to mount a remote file system on Linux (MAC)?

You can us programs like [Filezilla](https://filezilla-project.org/) or [Cyberduck](https://cyberduck.io/) (MAC) to transfer files between remote and local host. But sometimes it is much easier to mount a remote file system and work on it like an external drive.
You can use the `mountserver` ([Download: mountserver](mountserver)) script to do so. This requires `sshfs` to be installed, on Linux that is in the standard repositories. A short into and some additional steps can be found [here](https://www.tjansson.dk/2008/01/autofs-and-sshfs-the-perfect-couple/).

```bash
$ mountserver [remote] [remote directory] [local directory]
# example
$ mountserver login /srvfs/home/[user] /home/monkey/srvx1
```

This will mount the remote directory to the local directory. The local directory will be created if it does not exist. The directory should be empty before mounting, otherwise that will cause problems.

??? note "mountserver"

    ``` bash title="Mount a remote directory"
    --8<-- "SSH-VPN-VNC/mountserver"
    ```

## Q: How to use an SSH tunnel for private browsing?

based on a tutorial from [Linuxize](https://linuxize.com/post/how-to-setup-ssh-socks-tunnel-for-private-browsing/).

It can be really useful to access resources from inside the IMGW / UNIVIE network without using the VPN from the ZID. This can be done super easily. You need an SSH client (e.g. ssh, Putty) and [Firefox](https://www.mozilla.org/en-US/firefox/new/).

I'm showing the things here only for Linux, but Windows with Putty should be straight forward too. Connect to Aurora for example:

```bash
ssh -N -D 8091 [USER]@login.img.univie.ac.at
```

Options:

- `-N` - Tells SSH not to execute a remote command.
- `-D 8091` - Opens a SOCKS tunnel on the specified port number.
- To run the command in the background use the `-f` option.

Authenticate at the server and check that the connection is working. Next open Firefox and go to settings - network and select manual proxy configuration.

![](../mkdocs/img/ssh-tunnel-firefox.jpg)

**Voila.** You can access websites from within the UNIVIE / IMGW network.
