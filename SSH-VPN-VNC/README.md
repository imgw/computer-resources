# Connection 101

Please find here some details on how to connect using SSH or VPN or VNC. 

## General Access
Please use these methods to access the servers:

1. Connect using [SSH](SSH.md) or [MOSH](https://mosh.org/#) (Mobile Secure Shell)
2. Connect using [TeachingHub](../Students/TeachingHub.md) or [ResearchHub](../Servers/JET.md#jupyterhub)
3. Connect using [VNC](VNC.md)
4. Access from outside the UNI via the [VPN](VPN.md)

## Jupyterhub

Currently, there are two JupyterHub instances runnning:

- TeachingHub on [SRVX1](https://teaching.wolke.img.univie.ac.at)
- MasterHub on [SRVX1](https://quell.wolke.img.univie.ac.at)
- ResearchHub on [jet](https://jupyter.wolke.img.univie.ac.at)

Connect to either via the [wolke](https://wolke.img.univie.ac.at)

## Questions and Answers

- [Q: How does ssh work?](Questions.md#q-how-does-ssh-work)
- [Q: How to use ssh-key authentication?](Questions.md#q-how-to-use-ssh-key-authentication)
- [Q: How to use an ssh-agent?](Questions.md#q-how-to-use-an-ssh-agent)
- [Q: How to transfer files between two VPN networks?](Questions.md#q-how-to-transfer-files-between-two-vpn-networks)
- [Q: How to connect to IMGW Servers?](Questions.md#q-how-to-connect-to-jet-srvx8-srvx2)
- [Q: How to mount a remote file system on Linux (MAC)?](Questions.md#q-how-to-mount-a-remote-file-system-on-Linux-mac)

## Tools

Please find some useful tools for connecting to IMGW servers and University of Vienna VPN:

- Shell script using SSH to connect via a gateway, [SSH](SSH.md#connect-script) Download: [connect2jet](connect2jet)
- Shell script for F5FPC tools, [VPN](VPN.md#connect-script) Download: [connect2vpn](connect2vpn)
- Mount Server directories via sshfs, [SSHFS](SSH.md#sshfs)

??? note "connect2jet"

    ``` bash title="Connect to Jet"
    --8<-- "SSH-VPN-VNC/connect2jet"
    ```
    
??? note "connect2vpn"

    ``` bash title="Connect to VPN"
    --8<-- "SSH-VPN-VNC/connect2vpn"
    ```
