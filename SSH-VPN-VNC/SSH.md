# Secure Shell (SSH)

## Clients

on Linux and Mac, all tools are present. on Windows use one of these:

- [Bitvise SSH Client](https://www.bitvise.com/ssh-client-download) (for the SSH tunnel) 
- [MobaXterm](https://mobaxterm.mobatek.net)
- Windows subsystem Linux (WSL), [install](https://learn.microsoft.com/en-us/windows/wsl/install), then install e.g. Ubuntu and install the openssh.
- [VSCode](../Editors/vscode.md)
- Putty, Kitty, ...

## Connect

[How to connect from the Office](../Servers/README.md#how-to-connect-from-the-office) or [How to connect from abroad](../Servers/README.md#how-to-connect-from-home-or-abroad)

**Connect from the office** by typing either of the following in a terminal. Replace `[USERNAME]` with your own.

```bash title="ssh connections"
ssh [USERNAME]@login.img.univie.ac.at
ssh [USERNAME]@aurora.img.univie.ac.at
ssh [USERNAME]@jet01.img.univie.ac.at
ssh [USERNAME]@jet02.img.univie.ac.at
```
There are multiple options for the ssh client, please explore these by yourself if needed. This option is sometimes needed to forward a window to your local computer (Linux, or advanced windows users):

The `-X` option enables X11 forwarding via ssh, i.e., permits opening graphical windows. On Windows you need to enter these details to the ssh client.


Consider using a `~/.ssh/config` configuration file to allow easier access like this:

```sh title="./ssh/config"
Host *
    User [USERNAME]
    ServerAliveInterval 60
    ServerAliveCountMax 2

Host login
    HostName aurora.img.univie.ac.at

Host aurora
    HostName aurora.img.univie.ac.at

Host jet
    HostName jet01.img.univie.ac.at

Host login2jet
    HostName jet01.img.univie.ac.at
    ProxyJump login.img.univie.ac.at

```

and replacing `[USERNAME]` with your username. Using such a file allows to connect like this `ssh login` using the correct server adress and specified username. 

Please note the special algorithms for ecaccess and of course ECMWF uses [teleport](../ECMWF/README.md#connecting-to-ecmwf-services) now.

**From eduroam**: You should be able to log in as above.

**From the outer world**: use the [VPN](VPN.md) or `login.img.univie.ac.at` as jump host.

If you are a guest, you can apply for a [guest u:account](https://zid.univie.ac.at/en/uaccount/#c14154). This will give you access to eduroam and to the VPN. Your application needs to be endorsed by a staff member, who also determines the expiration date of the account. **Please ask the sponsor first!**

## SSH Authentication with keys

**Please add your ssh-keys via [IPA](IPA.md#add-ssh-key)**

Find a solution [Questions - How to use ssh-key authentication?](Questions.md#q-how-to-use-ssh-key-authentication) or [Questions - How to use an ssh-agent?](Questions.md#q-how-to-use-an-ssh-agent)

## Connect Script
If you are using a terminal (Mac, Linux, WSL, ...) you can use the script [Download: connect2jet](./connect2jet) like this:
```bash
connect2jet -g [Username]@login.img.univie.ac.at [Username]@jet01.img.univie.ac.at
```

??? note "connect2jet"

    ```bash title="Connect to Jet"
    --8<-- "SSH-VPN-VNC/connect2jet"
    ```

There is also an option to forward a port, e.g. the VNC Port:

```bash
connect2jet -g [Username]@login.img.univie.ac.at -p 5901 [Username]@jet01.img.univie.ac.at
```
which allows you to connect to `localhost:5901` and view the VNC session.

## Tunneling

If you are connected to eduroam or you are on an external computer, you'll need to use an SSH tunnel. The instructions below refer to jet01, but you can do just the same with jet02.

On Linux, start [Remmina](https://remmina.org/), then:

*   Set "Server" to `jet01.img.univie.ac.at:[DISPLAY]` in the "Basic" tab
*   Move to the "SSH Tunnel" tab, checkout "Enable SSH Tunnel", "Same server at port 22" and specify your favourite SSH authentication method.
*   Save and connect.

On Windows, you can use [Bitvise SSH Client](https://www.bitvise.com/ssh-client-download) (for the SSH tunnel) and the [RealVNC VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/windows/) or [MobaXterm](https://mobaxterm.mobatek.net).

Setup might be bit different for different clients, but all need these information:

Option Bitvise SSH Client/MobaXterm and RealVNC:

*   Start the SSH client
*   Go to tab "C2S" or SSH tunnels (port forwarding)
*   Set "Listen Interface" to `127.0.0.1`
*   Set "Listening Port" to `5900+[DISPLAY]`, e.g., `5905`
*   Set "Destination Host" to `jet01.img.univie.ac.at`
*   Set "Destination Port" to `5900+[DISPLAY]`
*   Now start VncViewer and connect to `127.0.0.1:5900+[DISPLAY]`

on Linux that is really simple:

```sh title="ssh port forwarding"
# REMOTEPORT -> to LOCALPORT
ssh LOCALPORT:localhost:REMOTEPORT USER@login
# connect to the local port
```

## SSH config
On Linux the ssh processes can use a file `$HOME/.ssh/config`, which looks like this

```sh title="ssh configuration"
Host *                                                                                                     
    IdentitiesOnly=yes
    ServerAliveInterval 60
    ServerAliveCountMax 20
    Compression yes
# will use any authentication possible (key, pass)
Host login
    Hostname login.img.univie.ac.at
    User [USERNAME]
# use a specific ssh-key for that connection
Host login-key
    Hostname login.img.univie.ac.at
    User [USERNAME]
    IdentityFile ~/.ssh/id_special
# force ssh to use password authentication
Host loginx
    HostName login.img.univie.ac.at
    User [USERNAME]
    PreferredAuthentications password
    PubkeyAuthentication no
# use login as a jump host to reach JET
Host login2jet1
    Hostname jet01.img.univie.ac.at
    User [USERNAME]
    ProxyJump login
```

and contains information on SSH connections. Look at the specific definitions from [ECMWF](../ECMWF/README.md#configuration).


## SSHFS
It is possible to mount your home directory to your personal computer on Linux via `sshfs` or using of course a dedicated remote file browser like: Filezilla, Cyberduck, ...

on Linux you need to install `fuse2` and `sshfs`, the names might vary between distributions, but are all in the default repos.
```bash
# connect to srvx1 using your home directory and a login directory on your local computer
# mountserver [host] [remotedir] [localdir]
mkdir -p $HOME/login
mountserver [USER]@login.img.univie.ac.at /srvfs/home/[USER] $HOME/srvx1
```


## SSH Banner
If you login in to any of the IMGW Servers you will be greeted by a banner showing some information about the system. However, after some time you might not need that information anymore.

run the following and you will never see the banner again.

```bash
touch $HOME/.hushlogin
```
remove that file and you shall see it again.
