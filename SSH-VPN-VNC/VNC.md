# VNC

**Be aware! Everyone with the VNC password will get access to your account**

It is recommended not to use VNC. Use **jupyterhub** or **screen** or **tmux** instead. However, for GUI applications there is no other way.

The VNC (Virtual Network Computing) allows to view a graphical user interface (GUI) from a remote server in an viewer application. This can be used to launch GUI programs on the servers.

Xvnc is the Unix VNC server. Applications can display themselves on Xvnc as if it were a normal display, but they will appear on any connected VNC viewers rather than on a physical screen. The VNC protocol uses the TCP/IP ports 5900+N, where N is the display number.

Currently VNC is installed on:
- JET01
- AURORA

## Userservices

It is highly recommended to use the userservices scripts available on all IMGW Servers to make configurations for VNC.

```bash title="using the userservices"
$ userservices vnc -h
################################################################################
User Services   -  VNC Server Setup/Launcher/Stopper

  vnc -h -s -x -d -l

Options:
    -h      Help
    -c      Check for vnc server(s) running
    -s      Stop vnc server(s)
    -x      Write xstartup in /home/spack/.vnc
    -d      Prepare vncserver Service
    -p []   Port: 1 - 99
    -l      Launch vnc server/service
    -w []   Desktop Session: icewm, xfce
################################################################################
Author: MB
Date: 25.01.2021
Path: /home/swd/userservices/userservices.d
################################################################################
Installed Desktops: icewm-session 
################################################################################
```

Running the script without any options will run all necessary steps. In case of error try removing your `.vnc` directory, as older configurations might be in the way. There shall be at least two desktop options: icewm and xfce. You can specify this directly with the `-w [DESKTOP]` option.


### Connecting

Use a VNC client (e.g. [TigerVNC Viewer](https://sourceforge.net/projects/tigervnc/files/stable/) (All OS) or [RealVNC VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/windows/) on Windows, or [Remmina](https://remmina.org/) on any Linux distribution) and connect to `login.img.univie.ac.at:[DISPLAY]`.

**Note: Since version 1.10 of tigerVNC it is possible to resize the windows directly**

Connect with a viewer:

1. Hostname: `login.img.univie.ac.at` or `jet01.img.univie.ac.at`
2. VNC Port: `userservices vnc`, but do not stop the server. Just get the port of a running server.


### Setting a window manager

This is more advanced. The VNC server will automatically start a graphical window manager/desktop environment. Details are stored in `.vnc/xstartup`. Your default configuration will load GNOME, and `.vnc/xstartup` will be something like this:

```bash
#!/bin/sh
 
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
/etc/X11/xinit/xinitrc
# Assume either Gnome will be started by default when installed
# We want to kill the session automatically in this case when user logs out. In case you modify
# /etc/X11/xinit/Xclients or ~/.Xclients yourself to achieve a different result, then you should
# be responsible to modify below code to avoid that your session will be automatically killed
if [ -e /usr/bin/gnome-session ]; then
    vncserver -kill $DISPLAY
fi
```

It is recommended to use IceWM instead. This is a more lightweight alternative to GNOME. To switch to IceWM, first log out from GNOME and disconnect from your VNC viewer, then change your `.vnc/xstartup` to this:

```bash
#!/bin/sh
 
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid grey
vncconfig -iconic &
xterm -geometry -sb -sl 500 -fn 9x15bold -title "$VNCDESKTOP Desktop" &
icewm &
```

Some information on what could be put into `.Xresources` is given [here](https://wiki.archlinux.org/title/x_resources). It might be possible to replace `icewm` here with `startxfce4` to choose XFCE Desktop environment.

## Change the resolution of your VNC Session

`xrandr` gives you a list of available resolutions, that can be use. Requires a `$DISPLAY` variable to be set, using your VNC display number does the trick, e.g. `:3`.

```bash
# Change VNC display resolution [width x height]
$ userservices vnc-geometry 1920x1080
```

Change the resolution to e.g. 1920x1080 (HD):
```bash
xrandr -s 1920x1080 -d $DISPLAY
```

Adding resolutions according to your display's resolution have a look at

??? note "add_xrandr_resolution"

    ``` bash title="Add a new monitor resolution to VNC"
    --8<-- "SSH-VPN-VNC/add_xrandr_resolution.sh"
    ```

and run it like this:

```bash title="Add a custom resolution to your virtual display"
# running the script and adding a resolution you require, in pixel
$ add_xrandr_resolution [width] [height]
```

Note: `$DISPLAY` is an environment variable that is usually set to your VNC server port.

