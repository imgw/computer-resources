# VPN

Requirements:

- `u:account`

Some servers are only accessible from within the virtual network of the university of Vienna. Therefore access from outside has to be granted via the [VPN-Service](https://vpn.univie.ac.at). Go there and login with your `u:account` and download the *Big-IP Edge* client for you system. 

![](https://zid.univie.ac.at/fileadmin/user_upload/d_zid/zid-open/daten/datennetz/vpn/Windows/01_download_neu.png) 

Links:

 * [ZID-VPN](https://vpn.univie.ac.at/f5-w-68747470733a2f2f7a69642e756e697669652e61632e6174$$/vpn/)
 * Linux (Ubuntu, Generic), Windows, Mac: [VPN user guides](https://vpn.univie.ac.at/f5-w-68747470733a2f2f7a69642e756e697669652e61632e6174$$/vpn/anleitungen/)
 * Arch based AUR package [AUR f5fpc](https://aur.archlinux.org/packages/f5fpc/) does not contain the correct f5fpc. You can download via `yay -Sw f5fpc`, replace `.deb` with `.deb` from ZID. Then build the package yourself: `makepkg --skipinteg -i`.

Follow the install instructions for Windows, Mac and Linux and make sure the software works.

![](https://zid.univie.ac.at/fileadmin/user_upload/d_zid/zid-open/daten/datennetz/vpn/Windows/08_verbinden.png)

On Windows and Mac you get a nice gui that requires you to fill in the VPN server: `vpn.univie.ac.at` and username and password from the `u:account`. On Linux execute the following:

```sh title="Big IP Edge Linux connection"
# split tunnel
f5fpc -s -t vpn.univie.ac.at -u [user]
# full tunnel (everything)

```
The status can be checked with `f5fpc --info`.

## Using Multi-factor Authentification (MFA)

When you enable MFA ([ZID-VPN-MFA](https://zid.univie.ac.at/en/vpn/#c41781)) on the [VPN login page](https://vpn.univie.ac.at), you will have to enter the TOTP (time-based one time password) during login:

```sh title="Big IP Edge Linux connection usinf MFA"
# using the TOTP code (6 digits)
f5fpc -s -t vpn.univie.ac.at -u [user]@[TOTP]
```

## Connect script

One can use the commands above or use the [connect2vpn](connect2vpn) script to connect to the University VPN service. Especially in Linux the interface is much more primitive than on Mac or Windows.

```bash
$ connect2vpn [u:account username]
[VPN] Using [u:account username] as username
[VPN] BIG-IP Edge Command Line Client version 7213.2021.0526.1 
[VPN] Full (1) or split (None) tunnel? (1/None):
```
Continue and wait until you get a response that it's connected.
The status stays visible. This will block your current terminal window.
