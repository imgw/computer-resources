#!/bin/bash
# By Michael Blaschek
# Date 15.08.2020
# CC BY 4.0 International
# University of Vienna, Austria

# Description:
# Add custom VNC resolution
# Requires: xrandr and a running VNC session

if [ $# -lt 2 ]; then
	echo "e.g. try: $0 1920 1380"
	echo "or 2560 1320"
else
	refresh=60
	if [ $# -eq 3 ]; then
		refresh=$3
	fi
	command=$(cvt $1 $2 $refresh | grep Modeline | awk '{for(i=3;i<NF;i++){printf "%s ",$i}}')
	xrandr --newmode  "$1x$2" ${command/Modeline/}
	xrandr --addmode VNC-0 "$1x$2"
	xrandr -d $DISPLAY -s "$1x$2"
fi

