#!/bin/bash
# By Michael Blaschek
# Date 23.03.2021
# CC BY 4.0 International
# University of Vienna, Austria

# Description:
# Connect a mount point from a remote server

CURRENT_MOUNTS=$HOME/.currentmounts

check(){
	status=0
	if [ -f ${CURRENT_MOUNTS} ]; then
		# check mount points
		while read line;
		do
			local_dir=$(echo "$line" | cut -d" " -f3)
			mount | grep $local_dir > /dev/null
			if [ $? -eq 0 ]; then
				status=1
				echo "$line" >> ${CURRENT_MOUNTS}.2
				echo "[CHECK] $local_dir [OK]"
			else
				echo "[CHECK] $local_dir [X]"
			fi
		done < ${CURRENT_MOUNTS}
		mv ${CURRENT_MOUNTS}.2 ${CURRENT_MOUNTS}
	fi
	return $status
}

fun_unmount(){
	if [ $# -eq 0 ]; then
		# unmount everything
		if [ -f ${CURRENT_MOUNTS} ]; then
			# check mount points
			# need to redirect file input to unit-3, so that STDIN is available for read again
			while read line <&3;
			do
				local_dir=$(echo "$line" | cut -d" " -f3)  
				fun_unmount ${local_dir}
			done 3<${CURRENT_MOUNTS}
			rm ${CURRENT_MOUNTS}
		fi
	else
		mount | grep $1 > /dev/null
		if [ $? -eq 0 ]; then
			local_dir=$(mount | grep $1 | tail -n1 | cut -d" " -f3)
			read -p "[UNMOUNT] ${local_dir} (y/n)?" REPLY
			case "$REPLY" in
				Y*|y*) fusermount -u ${local_dir}; echo "[UNMOUNT] ${local_dir} [OK]";;
				*) echo "[UNMOUNT] ${local_dir} [X]";;
    		esac
    	fi
	fi
}

fun_mount(){
	# Make directory
	mkdir -p $3
	# Mount using sshfs
	sshfs $1:$2 $3
	echo "$1 $2 $3" >> ${CURRENT_MOUNTS}
	echo "[MOUNT] $3 [OK]"
}
#
# Check if a mount point is active ?
#

sshfs -h 2> /dev/null 1> /dev/null
if [ $? -ne 0 ]; then
	echo "[MOUNT] sshfs is required to be installed."
	exit 0
fi

if [ $# -ne 3 ]; then
	check
	if [ $? -eq 0 ]; then
		echo "mountserver [host] [remotedir] [localdir]"
		exit 1
	fi
	echo "[MOUNT] unmounting ..."
	# unmount ?
	fun_unmount
else
	# mount
	host=$1
	host_dir=$2
	local_dir=$3
	fun_unmount ${local_dir}
	read -p "[MOUNT] ${local_dir} (y/n)?" REPLY
	case "$REPLY" in
	    Y*|y*) fun_mount $host $host_dir $local_dir;;
	    *) exit 0;;
	esac
fi

