# A U R O R A

> Research and Development Node

???+ info "Access"
    1. Request access / will be done for you by your supervisor.
    2. As Staff, access using SSH - [How to SSH / VNC / VPN](../SSH-VPN-VNC/README.md)
    3. Inform yourself: [Getting Started](../Getting%20Started.md)

Welcome to the HPC @IMG @UNIVIE and please follow these steps to become a productive member of our department and make good use of the computer resources. 
**Efficiency is keen.**


## System information

| Name        | Value                                                                              |
| ----------- | ---------------------------------------------------------------------------------- |
| Product     | [R282-Z96-00](https://www.gigabyte.com/at/Enterprise/Rack-Server/R282-Z96-rev-A00) |
| Processor   | AMD EPYC 7773X (Milan, Zen3) 64-Core Processor                                     |
| Cores       | 2 CPU, 64 physical cores per CPU, total 128 logical CPU units                      |
| CPU time    | 1.121 kh                                                                           |
| Memory      | 32x 64GB - 2048 GB Total                                                           |
| Memory/Core | 16 GB                                                                              |
| OS          | AlmaLinux 9.4 Seafoam Ocelot                                                       |
| Purchase    | June 2023                                                                          |
| OOB         | [Management](https://wolke.img.univie.ac.at/it.html)                               |
| IP          | 131.130.157.5                                                                      |
| C-Name      | `aurora.img.univie.ac.at`, `login.img.univie.ac.at`                                |


```title='Aurora Borealis'
-----------------------------------------------
  |  |+|: | : : :|   .        `              .
    |  | :| : | : |:   |  .  131.130.157.5
     ` || | : | |: : |   .  `           .   :.
   *    ` : | | :| |*|  :   :               :|
  |  .      ` ' :| | :| . : :         *   :.||
  |  : .:|       ` | || | : |: |          | ||
  |  :  .: .         '| | : :| :    .   |:| ||
  ` *|  || :       `    | | :| | :      |:| |
        || |.: *          | || : :     :|||
  .   .  ` |||.  +        + '| |||  .  ||`
    .     +:`|!             . ||||  :.||`
           ..!|*          . | :`||+ |||`
 +      : |||`        .| :| | | |.| ||`     .
   +  :|| |`     :.+. || || | |:`|| `
     .||` .    ..|| | |: '` `| | |`  +
     ||        !|!: `       :| |
 .    | .      `|||.:      .||    .      .    `
        `|.   .  `:|||   + ||'     `
           `'       `'|.    `:
`^``----.,.___          `.    `.  .    ____,.,-
      ^       """'---,..___ __,..---""'
 ^ AURORA                  ``--..,__
-----------------------------------------------
```

The SRV Storage System (2x 400 TB raid protected storage array) is connected to Aurora, as well as the SSD Storage system (11 TB raid protected SSD storage array).

Paths:

  - `/srvfs/home/[username]`
  - `/srvfs/scratch/[username]`
  - `/srvfs/tmp/[username]`
  - `/srvfs/data`
  - `/srvfs/shared`
  - `/srvfs/webdata`
  - `/srvfs/fastscratch/[username]`  (SSD)


## Software

The typcial installation of a server has the INTEL Compiler suite (`intel-oneapi`), the open source GNU Compilers and AMD compilers (`aocc`) installed. Based on these three different compilers (`intel`, `gnu`, `aocc`), there are usually two version of each scientific software.

Major Libraries:

- OpenMPI (4.0.5, 4.1.5)
- HDF5
- NetCDF (C, Fortran)
- ECCODES from [ECMWF](https://confluence.ecmwf.int/display/ECC)
- Math libraries e.g. intel-mkl, lapack, scalapack, amd-aocl, ...
- Interpreters: Python, Julia
- Tools: cdo, ncl, nco, ncview

These software libraries are usually handled by environment modules.

![](../mkdocs/img/envmodules.png)

## Currently installed modules

Please note that new versions might already be installed.

```bash title="available modules"
$ module av
---------- /home/swd/spack/share/spack/modules/linux-almalinux9-zen3 -----------
amd-aocl/4.0-aocc-4.0.0-o36ia3m
amdblis/4.0-aocc-4.0.0-6ixdyat
amdfftw/4.0-aocc-4.0.0-kc6bebp
amdlibflame/4.0-aocc-4.0.0-folsmzp
amdlibm/4.0-aocc-4.0.0-q2t5i6c
amdscalapack/4.0-aocc-4.0.0-ggh7q55
anaconda2/2019.10-gcc-11.3alma9-fcgjmgb
anaconda3/2022.05-gcc-11.3alma9-6zg2qhl
aocc/4.0.0-o2qhi24
aocl-sparse/4.0-aocc-4.0.0-nsbrnq3
cdo/2.1.1-gcc-11.3.0-vmby32i
cdo/2.1.1-intel-2021.7.1-lj7cqwd
diffutils/3.8-intel-2021.7.1-3zg5zz5
eccodes/2.25.0-gcc-11.3.0-nomemfs
eccodes/2.25.0-gcc-11.3.0-ufrmjdi
eccodes/2.25.0-intel-2021.7.1-zq4yw5j
gcc/11.3.0-wc3zw5d
geos/3.11.2-gcc-11.3.0-2r7g4lv
geos/3.11.2-intel-2021.7.1-uu3t5pe
hdf5/1.10.7-gcc-11.3.0-xuf5zy3
hdf5/1.12.2-aocc-4.0.0-blufisu
hdf5/1.12.2-intel-2021.7.1-xcicrrh
intel-oneapi-compilers-classic/2021.7.1-o2gpvj3
intel-oneapi-compilers/2022.2.1-zxlluqk
intel-oneapi-mkl/2023.1.0-gcc-11.3.0-rljcxmq
intel-oneapi-mkl/2023.1.0-intel-2021.7.1-t2h2yxz
intel-oneapi-mpi/2021.9.0-intel-2021.7.1-jzufygb
intel-oneapi-vtune/2023.1.0-intel-2021.7.1-rltylzk
miniconda2/4.7.12.1-gcc-11.3alma9-j3jkilu
miniconda3/4.10.3-gcc-11.3alma9-nin3ujf
nco/5.1.0-intel-2021.7.1-yatuzww
nco/5.1.5-gcc-11.3.0-hyaiqja
ncview/2.1.8-gcc-11.3.0-u5cproj
ncview/2.1.8-intel-2021.7.1-kol7zhq
netcdf-c/4.7.4-aocc-4.0.0-2evbx66
netcdf-c/4.7.4-gcc-11.3.0-t54zl2r
netcdf-c/4.7.4-intel-2021.7.1-6ltfavb
netcdf-c/4.9.2-gcc-11.3.0-pbnofsp
netcdf-fortran/4.5.3-gcc-11.3.0-2n4urj3
netcdf-fortran/4.5.3-intel-2021.7.1-qtjfan7
netlib-lapack/3.11.0-gcc-11.3.0-t4mngkc
netlib-scalapack/2.2.0-gcc-11.3.0-rxwa7sf
openblas/0.3.23-gcc-11.3.0-v4jxkqk
openmpi/4.0.5-gcc-11.3.0-pob76hu
openmpi/4.1.5-aocc-4.0.0-hwgfdau
proj/9.2.0-gcc-11.3.0-2krnxcr
proj/9.2.0-intel-2021.7.1-cxnmlak

------------------------------ /home/swd/modules -------------------------------
gnu-stack/gcc.11.3.0        micromamba/1.4.3  ncl/6.6.2
intel-stack/intel-2021.7.1  mojo/0.5.0        teleport/10.3.3
```

### AMD Compilers

The AMD Optimizing C/C++ and Fortran Compilers (“AOCC”) are a set of production compilers optimized for software performance when running on AMD host processors using the AMD “Zen” core architecture. The AOCC compiler environment simplifies and accelerates development and tuning for x86 applications built with C, C++, and Fortran languages.

There are customized versions of

- **LibM** is a software library containing a collection of basic math functions optimized for x86-64 processor-based machines.
- **FFTW** (AMD Optimized version) is a comprehensive collection of fast C routines for computing the Discrete Fourier Transform (DFT)
- **BLIS** is a portable software framework for instantiating high-performance BLAS-like dense linear algebra libraries.
- \*_libFLAME_ (AMD Optimized version) is a portable library for dense matrix computations, providing much of the functionality present in Linear Algebra Package (LAPACK).
- **ScaLAPACK** is a library of high-performance linear algebra routines for parallel distributed memory machines.
- AMD Optimizing CPU Libraries (AOCL) - **AOCL** is a set of numerical libraries tuned specifically for AMD EPYC processor family.
- **AOCL-Sparse** is a library that contains basic linear algebra subroutines for sparse matrices and vectors optimized for AMD EPYC family of processors.

```sh
$ module av | grep aocc
amd-aocl/4.0-aocc-4.0.0-o36ia3m
amdblis/4.0-aocc-4.0.0-6ixdyat
amdfftw/4.0-aocc-4.0.0-kc6bebp
amdlibflame/4.0-aocc-4.0.0-folsmzp
amdlibm/4.0-aocc-4.0.0-q2t5i6c
amdscalapack/4.0-aocc-4.0.0-ggh7q55
aocc/4.0.0-o2qhi24
aocl-sparse/4.0-aocc-4.0.0-nsbrnq3
hdf5/1.12.2-aocc-4.0.0-blufisu
netcdf-c/4.7.4-aocc-4.0.0-2evbx66
openmpi/4.1.5-aocc-4.0.0-hwgfdau
```

on how to use environment modules go to [Using Environment Modules](../Misc/Environment-Modules.md)

## User services

There is a script collection that is accessible via the `userservices` command. e.g. running

```bash
$ userservices

Usage: userservices [service] [Options]
Available Services:
------------------------------------------------------------------------
 containers           --- Show available containers
 fetch-sysinfo        --- Display system information
 filesender           --- Transfer files to ACONET filesender (requires account)
 fix-permissions      --- fix file/directory permissions
 limits               --- Report user processes and limits
 modules              --- list environment modules
 numfiles             --- Check number of files in all sub directories
 quota                --- Report user quotas
 server-schedule      --- Show Server schedule
 sshtools             --- SSH agent user commands
 transfersh           --- Transfer files/directories (IMGW subnet)
 ucloud               --- Upload files/directories to your u:cloud
 vnc                  --- VNC Server Setup/Launcher/Stopper
 vnc-geometry         --- Change geometry of VNC display
 weather              --- Retrieve weather information
 yopass               --- Send messages/small files to YoPass (encrypted)
------------------------------------------------------------------------
These scripts are intended to help with certain known problems/tasks.
Report problems to: michael.blaschek@univie.ac.at

```

These are scripts in a common directory. Feel free to copy or edit as you like. Note that some services like `filesender` require an [ACONET](https://filesender.aco.net/) account (accessible via your [u:account](https://zid.univie.ac.at/my-uaccount/)).
