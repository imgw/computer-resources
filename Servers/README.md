# Servers

The Department of Meteorology and Geophysics has access to the following computing resources:

- Teaching Server [SRVX1](SRVX1.md)
- Development Server [Aurora](Aurora.md)
- Services Server [SRVX8](SRVX8.md)
- Computing Cluster [JET](JET.md)
- Vienna Scientific Cluster [VSC](../VSC.md)

## Available Work Environments

Locations:

- Students [SRVX1](SRVX1.md), [TeachingHub + MasterHub](../Students/TeachingHub.md)
- Staff Development [Aurora](Aurora.md)
- Staff + Remote Desktop + Jupyterhub [Jet Cluster](JET.md)
- Staff [Vienna Scientific Cluster (VSC)](../VSC.md)
  - [VSC Training](https://vsc.ac.at/training)
  - [VSC Trainings @ IMGW](https://gitlab.phaidra.org/imgw/trainings-course)

_Note: Please take a look at the training course @VSC. There might be a beginners course about Linux or Clusters or Python!_ It is highly recommended to use these existing courses to get up to speed. Most of these are available as pdfs right away, but still taking the course is the prefered way.

## Access to different servers

<!-- <center> -->
  <table class="styled-table">
    <thead>
      <tr>
        <th title="Field #1">server</th>
        <th title="Field #2">access</th>
        <th title="Field #3">purpose</th>
        <th title="Field #4">comment</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>TeachingHub (SRVX1)</td>
        <td><b>u</b>:account</td>
        <td>students</td>
        <td>access via moodle</td>
      </tr>
      <tr>
        <td>MasterHub (SRVX1)</td>
        <td><b>w</b>:account</td>
        <td>masters</td>
        <td>access by supervisor</td>
      </tr>
      <tr>
        <td>Aurora</td>
        <td><b>w</b>:account</td>
        <td>masters, staff, external</td>
        <td>access by supervisor or wiki request</td>
      </tr>
      <tr>
        <td>JET</td>
        <td><b>w</b>:account</td>
        <td>masters, staff, external</td>
        <td>access by supervisor or wiki request</td>
      </tr>
      <tr>
        <td>ECMWF</td>
        <td><b>ec</b>:account</td>
        <td>staff</td>
        <td>ECMWF Member state, contact supervisor</td>
      </tr>
      <tr>
        <td>Vienna Scientific Cluster (VSC)</td>
        <td><b>v</b>:account</td>
        <td>masters, staff</td>
        <td>access by supervisor</td>
      </tr>
    </tbody>
  </table>
  <style>
    .styled-table {
      border-collapse: collapse;
      margin: 25px 0;
      font-size: 0.9em;
      font-family: monospace;
      min-width: 400px;
      box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
    }

    .styled-table thead tr {
      background-color: #0059a4;
      color: #ffffff;
      text-align: left;
    }

    .styled-table th,
    .styled-table td {
      padding: 12px 15px;
    }

    .styled-table tbody tr {
      border-bottom: 1px solid #dddddd;
    }

    .styled-table tbody tr:nth-of-type(even) {
      background-color: #f3f3f3;
    }

    .styled-table tbody tr:last-of-type {
      border-bottom: 2px solid #0059a4;
    }

    .styled-table tbody tr.active-row {
      font-weight: bold;
      color: #0059a4;
    }
  </style>
<!-- </center> -->


## How to connect from Home or Abroad?

![IMGW Connection Diagramm](../mkdocs/img/IMGW-connection-diagram.png)

## How to connect from the Office?

![IMGW Connection Diagramm2](../mkdocs/img/IMGW-connection-diagram2.png)

## Services

Summary of services available @IMGW for staff and students. Some of these services require different accounts ([info](https://wolke.img.univie.ac.at/accounts.html)) or nework access.

| name        | description                           | url/path                                                                        | access  | host  |
| ----------- | ------------------------------------- | ------------------------------------------------------------------------------- | ------- | ----- |
| teachinghub | Jupyterlab for Teaching & Development | [teaching.wolke.img.univie.ac.at](https://teaching.wolke.img.univie.ac.at)      | web     | SRVX1 |
| MasterHub   | Jupyterlabfor Master Students         | [quell.wolke.img.univie.ac.at](https://quell.wolke.img.univie.ac.at)            | web     | SRVX1 |
| ecgateway   | ECMWF Member State Gateway            | [ecaccess.wolke.img.univie.ac.at](https://ecaccess.wolke.img.univie.ac.at/)     | img/vpn | JET02 |
| researchhub | Jupyterlab for Development            | [jupyter.wolke.img.univie.ac.at](https://jupyter.wolke.img.univie.ac.at)        | img/vpn | JET02 |
| webdata     | File server (`/srvfs/webdata`)        | [webdata.wolke.img.univie.ac.at](https://webdata.wolke.img.univie.ac.at)        | web     | SRVX8 |
| YoPass      | Secure Message Service (YoPass)       | [secure.wolke.img.univie.ac.at](https://secure.wolke.img.univie.ac.at/)         | web     | SRVX8 |
| TransferSH  | Easy commandline file transfer/send   | [transfersh.wolke.img.univie.ac.at](https://transfersh.wolke.img.univie.ac.at/) | web     | SRVX8 |
| Uptime      | Server Monitoring                     | [uptime.wolke.img.univie.ac.at](https://uptime.wolke.img.univie.ac.at/)         | web     | SRVX8 |
| IPA         | User Management                       | [wolke.img.univie.ac.at/ipa/ui](https://wolke.img.univie.ac.at/ipa/ui)          | img/vpn | WOLKE |


## Containers

🚧🚧🚧🚧🚧🚧(ongoing)🚧🚧🚧🚧🚧

Summary of containerized applications available @IMGW.

Containerization is done via singularity/apptainer. A some what incomplete introduction is given [here](https://gitlab.phaidra.org/imgw/singularity).


