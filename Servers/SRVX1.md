
# S R V X 1

> Teaching Hub and Master Hub from the web

???+ info "Access"
    1. Students access via Moodle - [How to connect using the TeachingHub](../Students/TeachingHub.md)
    2. Master students via [MasterHub](https://quell.wolke.img.univie.ac.at). Supervisor requests access.
    
    no SSH access, use Jupyter terminal.


## System information
| Name        | Value                                                          |
| ----------- | -------------------------------------------------------------- |
| Product     | PowerEdge R940                                                 |
| Processor   | Intel(R) Xeon(R) Gold 6148 (Skylake) CPU @ 2.40GHz             |
| Cores       | 4 CPU,  20 physical cores per CPU, total 160 logical CPU units |
| CPU time    | 700 kh                                                         |
| Memory      | 48x 16GB - 768 GB Total                                        |
| Memory/Core | 9.6 GB                                                         |
| OS          | RedHatEnterprise 8.10 Ootpa                                    |
| Purchase    | June 2018                                                      |
| OOB         | [Management](https://wolke.img.univie.ac.at/it.html)           |
| IP          | 131.130.157.11                                                 |
| C-Name      | `srvx1.img.univie.ac.at`                                       |


``` title="Mountain Greeter"
----------------------------------------------
 131.130.157.11 _    .  ,   .           .
    *  / \_ *  / \_      _  *        *   /\'_
      /    \  /    \,   ((        .    _/  / 
 .   /\/\  /\/ :' __ \_  `          _^/  ^/  
    /    \/  \  _/  \-'\      *    /.' ^_   \
  /\  .-   `. \/     \ /==~=-=~=-=-;.  _/ \ -
 /  `-.__ ^   / .-'.--\ =-=~_=-=~=^/  _ `--./
/SRVX1   `.  / /       `.~-^=-=~=^=.-'      '
----------------------------------------------
```

## Software

The typcial installation of a intel-server has the INTEL Compiler suite (`intel-parallel-studio`, `intel-oneapi`) and the open source GNU Compilers installed. Based on these two different compilers (`intel`, `gnu`), there are usually two version of each scientific software.

Major Libraries:

 - OpenMPI (3.1.6, 4.0.5)
 - HDF5 
 - NetCDF (C, Fortran)
 - ECCODES from [ECMWF](https://confluence.ecmwf.int/display/ECC)
 - Math libraries e.g. intel-mkl, lapack,scalapack
 - Interpreters: Python, Julia
 - Tools: cdo, ncl, nco, ncview

These software libraries are usually handled by environment modules.

![](../mkdocs/img/envmodules.png)

## Currently installed modules
Please note that new versions might already be installed.

```bash title="available modules"
module av
------- /home/swd/spack/share/spack/modules/linux-rhel8-skylake_avx512 -------
anaconda2/2019.10-gcc-8.5.0                         proj/7.1.0-gcc-8.5.0
anaconda3/2020.11-gcc-8.5.0                         proj/8.1.0-gcc-8.5.0
anaconda3/2021.05-gcc-8.5.0                         python/3.8.12-gcc-8.5.0
autoconf/2.69-oneapi-2021.2.0
autoconf/2.71-oneapi-2021.2.0
cdo/1.9.10-gcc-8.5.0
eccodes/2.19.1-gcc-8.5.0
eccodes/2.19.1-gcc-8.5.0-MPI3.1.6
eccodes/2.19.1-intel-20.0.4
eccodes/2.19.1-intel-20.0.4-MPI3.1.6
eccodes/2.21.0-gcc-8.5.0
eccodes/2.21.0-gcc-8.5.0-MPI3.1.6
eccodes/2.21.0-intel-20.0.4
fftw/3.3.10-gcc-8.5.0
fftw/3.3.10-gcc-8.5.0-MPI3.1.6
gcc/8.5.0-gcc-8.5rhel8
geos/3.8.1-gcc-8.5.0
hdf5/1.10.7-gcc-8.5.0
hdf5/1.10.7-gcc-8.5.0-MPI3.1.6
hdf5/1.10.7-intel-20.0.4-MPI3.1.6
hdf5/1.12.0-intel-20.0.4
hdf5/1.12.0-oneapi-2021.2.0
intel-mkl/2020.4.304-intel-20.0.4
intel-oneapi-compilers/2021.2.0-oneapi-2021.2.0
intel-oneapi-dal/2021.2.0-oneapi-2021.2.0
intel-oneapi-mkl/2021.2.0-oneapi-2021.2.0
intel-oneapi-mpi/2021.2.0-oneapi-2021.2.0
intel-parallel-studio/composer.2020.4-intel-20.0.4
libemos/4.5.9-gcc-8.5.0-MPI3.1.6
libemos/4.5.9-intel-20.0.4
libgeotiff/1.6.0-intel-20.0.4
matlab/R2020b-gcc-8.5.0
miniconda2/4.7.12.1-gcc-8.5.0
miniconda3/4.10.3-gcc-8.5.0
nco/4.9.3-intel-20.0.4
nco/5.0.1-gcc-8.5.0
ncview/2.1.8-gcc-8.5.0
ncview/2.1.8-intel-20.0.4-MPI3.1.6
netcdf-c/4.6.3-gcc-8.5.0-MPI3.1.6
netcdf-c/4.6.3-intel-20.0.4-MPI3.1.6
netcdf-c/4.7.4-gcc-8.5.0
netcdf-c/4.7.4-intel-20.0.4
netcdf-fortran/4.5.2-gcc-8.5.0-MPI3.1.6
netcdf-fortran/4.5.2-intel-20.0.4-MPI3.1.6
netcdf-fortran/4.5.3-gcc-8.5.0
netlib-lapack/3.9.1-gcc-8.5.0
netlib-lapack/3.9.1-intel-20.0.4
netlib-lapack/3.9.1-oneapi-2021.2.0
netlib-scalapack/2.1.0-gcc-8.5.0
netlib-scalapack/2.1.0-gcc-8.5.0-MPI3.1.6
openblas/0.3.18-gcc-8.5.0
opencoarrays/2.7.1-gcc-8.5.0
opencoarrays/2.7.1-intel-20.0.4
openmpi/3.1.6-gcc-8.5.0
openmpi/3.1.6-intel-20.0.4
openmpi/4.0.5-gcc-8.5.0
openmpi/4.0.5-intel-20.0.4
parallel-netcdf/1.12.2-gcc-8.5.0
parallel-netcdf/1.12.2-gcc-8.5.0-MPI3.1.6

----------------------------- /home/swd/modules ------------------------------
anaconda3/leo-current-gcc-8.3.1  intelpython/2021.4.0.3353  pypy/7.3.5       
ecaccess-webtoolkit/4.0.2        intelpython/2022.0.2.155   shpc/0.0.33      
ecaccess-webtoolkit/6.3.1        micromamba/0.15.2          teleport/10.1.4  
enstools/v2021.11                micromamba/0.27.0          teleport/10.3.3  
idl/8.2-sp1                      ncl/6.6.2                  xconv/1.94  
```
on how to use environment modules go to [Using Environment Modules](../Misc/Environment-Modules.md)
