# S R V X 8

> Services and Virtual Machines

???+ warning  "Access"
    no public access

## System Information

| Name        | Value                                                         |
| ----------- | ------------------------------------------------------------- |
| Product     | PowerEdge R730xd                                              |
| Processor   | Intel(R) Xeon(R) CPU E5-2697 v3 (Haswell) @ 2.60GHz           |
| Cores       | 2 CPU,  14 physical cores per CPU, total 56 logical CPU units |
| CPU time    | 245 kh                                                        |
| Memory      | 12x 16GB, 12x 32GB - 576 GB Total                             |
| Memory/Core | 18 Gb                                                         |
| OS          | AlmaLinux 8.10 Cerulean Leopard                               |
| Purchase    | Oktover 2015                                                  |
| OOB         | [Management](https://wolke.img.univie.ac.at/it.html)          |
| IP          | 131.130.157.8                                                 |
| C-Name      | `srvx8.img.unvie.ac.at`                                       |


``` title="Cloud Greeter"
----------------------------------------------
                 _
              (`  ).
             (     ).
)           _( SRVX8 '`.
        .=(`(      .   )     .--
       ((    (..__.:'-'   .+(   )
`.     `(       ) )       (   .  )
  )      ` __.:'   )     (   (   ))
)  )  ( )       --'       `- __.'
.-'  (_.'          .')
                  (_  )
                              131.130.157.8
--..,___.--,--'`,---..-.--+--.,,-,,.-..-._.-.-
----------------------------------------------
```

## Services

There are a number of services running that are just forwarded to the WOLKE. Find some more information in [services](README.md#services)
