# Students

To make you life easier we have listed a few things here that you should know to make good use of the IT services at our department.

## Guidelines

Access to computational resources at IMGW is granted to the user by employment at the university or by a IMGW sponsor. Access is disabled on the termination date without further notice. An extension of the allocation may be negotiated with the responsible Admin or IMGW sponsor. The user accepts the following responsibilities:

- Computers and information systems must be used in an ethical and legal manner.
- The user agrees not to duplicate or use copyrighted or proprietary software without proper authorization. 
- The user may not use computers and information systems in any manner for any business, professional, or other activity that is unrelated to the purpose of the resource allocation.
- The user is required to acknowledge the use of IMGW resources in any resulting publications.
- The user is responsible for protecting her/his access credentials and/or passwords.
- The user may not share her/his account privileges with anyone or knowingly permit any unauthorized access to a computer, computer privileges, systems, networks, or programs. The accounts of those involved will be disabled if sharing is detected.
- The user is responsible for backing up critical data to protect it against loss or corruption. The user is also responsible for understanding the usage and data retention policies for the file system and data archive resources used.
- The user agrees to report potential security breaches as soon as possible to the responsible Admin or IMGW sponsor.
- The user is responsible for ensuring that IMGW has her/his current contact information, including phone number, email address, and mailing address. If the user’s name, phone number, email address, or other information changes, the responsible Admin or IMGW sponsor must be promptly notified.


## Master Students

Great idea to start a master @ IMGW. Your supervisor will request a server account and you will retrieve login credentials. Please take a look at the [guidelines](#guidelines) above and acknowledge them.

As an employee or master student you can get access to these resources:

 - access to [SRVX1](../Servers/SRVX1.md)
 - access to [JET](../Servers/JET.md)
 - access to [VSC](../VSC.md)
 - acccess to [ECMWF](../ECMWF/README.md)

Most people at the department use [gitlab](https://gitlab.phaidra.org) and [mattermost](https://discuss.phaidra.org), which your supervisor can arange for you to get an account.

It might be useful to familiarize yourself with [git](../Git/README.md) and create a project dedicated to your master thesis on gitlab. Your supervisor can help you. Writing your thesis can be fascilitated by using [Overleaf](https://www.overleaf.de) and a [template](https://www.overleaf.com/read/ptpskhdqmqpt#566d55). As student at the University of Vienna, you can login with your u:account. [Overleaf Limits](https://www.overleaf.com/learn/how-to/Overleaf_plan_limits) and [Timeout Compile Tips](https://www.overleaf.com/learn/how-to/Fixing_and_preventing_compile_timeouts )
More template from our department can be found [here (wiki)](https://wiki.univie.ac.at/x/VBURC)


## Useful links

- [VSC Compiling](../mkdocs/present/Presentation.html?file=compiling.md)
- [IT @ IMG Overview](../mkdocs/present/Presentation.html?file=IT-Introduction.md)
