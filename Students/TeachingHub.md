# Jupyter Notebook Lab
![](../mkdocs/img/jupyter-logo.png){: width="300px"}

![](../mkdocs/img/Masterhub_logo.png){: width="200px" align=right}

## MasterHub

Students need to get a server account for accessing this resource. However, it is intended to help students complete their Masters and have an easy accessible Jupyterhub. Your Supervisor can ask for an account for you.

[Master Hub](https://quell.wolke.img.univie.ac.at)

## TeachingHub

![](../mkdocs/img/moodlelogo_small.png){: width="200px" align=right}

???+ warning "Annual purge of HOME"
    It is necessary for a smooth operation to purge the contents of the TeachingHub on an annual basis. Every year (**23. September**) before the winter semester starts there will be a purge of students home directories.

What is available?

Authentication is only possible via moodle. Every student can use their u:account to login into moodle and be forwarded to the new teaching hub.
Access via [Moodle lecture](https://moodle.univie.ac.at/my/) and Jupyterhub Symbol.
Access after successful login via Moodle: [teaching.wolke.img.univie.ac.at](https://teaching.wolke.img.univie.ac.at)

![TeachingHub access via Moodle](../mkdocs/img/teachinghub-new-moodle.png)

Just click on your Jupyterhub icon and you will be forwarded to the teaching hub:

![TeachingHub welcome](../mkdocs/img/teachinghub-new-welcome.png)

There should be a `Welcome.ipynb` waiting for you and you are free to try out different kernels.

![TeachingHub jupyter kernel selection](../mkdocs/img/teachinghub-new-kernels.png)

It is also possible to use a VNC (Virtual Desktop) to open installed gui applications, such as `ncview`. However, that is quite special and will only be required to be used in certain lectures.

![TeachingHub vnc desktop](../mkdocs/img/teachinghub-new-vnc.png)

