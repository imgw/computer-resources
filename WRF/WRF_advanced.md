## Advanced usage

### Changing the source code

### Conditional compilation

Most Fortran compilers allow passing the source code through a C preprocessor (CPP; sometimes also called the Fortran preprocessor, FPP) to allow for conditional compilation. In the C programming language, there are some directives that make it possible to compile portions of the source code  selectively.

In the WRF source code, Fortran files have an .F extension. cpp will parse these files and create corresponding .f90 files. The .f90 files will then be compiled by the Fortran compiler.

This means:
1. When editing the source code, always work on the .F files, otherwise changes will be lost on the next compilation.
2. In the .F files, it is possible to include `#ifdef` and `#ifndef` directives for conditional compilation.

For instance, in `dyn_em/module_initialize_ideal.F`, the following bits of code define the model orography for idealized large-eddy simulation runs. Four possibilities are given: `MTN`, `EW_RIDGE`, `NS_RIDGE`, and `NS_VALLEY`. If none is selected at compile time (select by adding `!` in front of #ifdef and #endif), none of these code lines is compiled and `grid%ht(i,j)` (the model orography) is set to 0:

```fortran
#ifdef MTN
  DO j=max(ys,jds),min(ye,jde-1)
  DO i=max(xs,ids),min(xe,ide-1)
     grid%ht(i,j) = mtn_ht * 0.25 * &
               ( 1. + COS ( 2*pi/(xe-xs) * ( i-xs ) + pi ) ) * &
               ( 1. + COS ( 2*pi/(ye-ys) * ( j-ys ) + pi ) )
  ENDDO
  ENDDO
#endif
#ifdef EW_RIDGE
  DO j=max(ys,jds),min(ye,jde-1)
  DO i=ids,ide
     grid%ht(i,j) = mtn_ht * 0.50 * &
               ( 1. + COS ( 2*pi/(ye-ys) * ( j-ys ) + pi ) )
  ENDDO
  ENDDO
#endif
#ifdef NS_RIDGE
  DO j=jds,jde
  DO i=max(xs,ids),min(xe,ide-1)
     grid%ht(i,j) = mtn_ht * 0.50 * &
               ( 1. + COS ( 2*pi/(xe-xs) * ( i-xs ) + pi ) )
  ENDDO
  ENDDO
#endif
#ifdef NS_VALLEY
  DO i=ids,ide
  DO j=jds,jde
     grid%ht(i,j) = mtn_ht
  ENDDO
  ENDDO
  xs=ids   !-1
  xe=xs + 20000./config_flags%dx
  DO j=jds,jde
  DO i=max(xs,ids),min(xe,ide-1)
     grid%ht(i,j) = mtn_ht - mtn_ht * 0.50 * &
               ( 1. + COS ( 2*pi/(xe-xs) * ( i-xs ) + pi ) )
  ENDDO
  ENDDO
#endif
```

To control conditional compilation:
1. Search for the variable `ARCHFLAGS` in `configure.wrf`
2. Add the desired define statement at the bottom. For instance, to selectively compile the `NS_VALLEY` block above, do the following:

```Makefile
ARCHFLAGS       =    $(COREDEFS) -DIWORDSIZE=$(IWORDSIZE) -DDWORDSIZE=$(DWORDSIZE) -DRWORDSIZE=$(RWORDSIZE) -DLWORDSIZE=$(LWORDSIZE) \
                     $(ARCH_LOCAL) \
                     $(DA_ARCHFLAGS) \
                      -DDM_PARALLEL \
...
                      -DNMM_NEST=$(WRF_NMM_NEST) \
                      -DNS_VALLEY

```

### Customizing model output

### Adding namelist variables

### Running offline nested simulations

### Running LES with online computation of resolved-fluxes turbulent fluxes

WRFlux
