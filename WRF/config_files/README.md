# Configuration files

??? note "configure.wrf.v4.4.2.srvx1.dmpar"

    ``` sh title="configure.wrf.v4.4.2.srvx1.dmpar"
    --8<-- "WRF/config_files/configure.wrf.v4.4.2.srvx1.dmpar"
    ```

??? note "configure.wrf.v4.4.2.srvx1.serial"

    ``` sh title="configure.wrf.v4.4.2.srvx1.serial"
    --8<-- "WRF/config_files/configure.wrf.v4.4.2.srvx1.serial"
    ```

??? note "configure.wrf.v4.4.2.srvx1.serial.debug"

    ``` sh title="configure.wrf.v4.4.2.srvx1.serial.debug"
    --8<-- "WRF/config_files/configure.wrf.v4.4.2.srvx1.serial.debug"
    ```

??? note "configure.wrf.v4.4.2.vsc5.gfortran"

    ``` sh title="configure.wrf.v4.4.2.vsc5.gfortran"
    --8<-- "WRF/config_files/configure.wrf.v4.4.2.vsc5.gfortran"
    ```

??? note "configure.wrf.v4.4.vsc5.intel"

    ``` sh title="configure.wrf.v4.4.vsc5.intel"
    --8<-- "WRF/config_files/configure.wrf.v4.4.vsc5.intel"
    ```

??? note "modules_jet_intel.sh"

    ``` sh title="modules_jet_intel.sh"
    --8<-- "WRF/config_files/modules_jet_intel.sh"
    ```

??? note "modules_srvx1_intel.sh"

    ``` sh title="modules_srvx1_intel.sh"
    --8<-- "WRF/config_files/modules_srvx1_intel.sh"
    ```

??? note "modules_vsc5_gfortran.sh"

    ``` sh title="modules_vsc5_gfortran.sh"
    --8<-- "WRF/config_files/modules_vsc5_gfortran.sh"
    ```

??? note "modules_vsc5_intel.sh"

    ``` sh title="modules_vsc5_intel.sh"
    --8<-- "WRF/config_files/modules_vsc5_intel.sh"
    ```
