#!/usr/bin/env python3
import os

list = os.listdir('.')
pwd = 'WRF/config_files'
file = open('README.md', 'w')

file.write("# Configuration files\n")

# ??? note "connect2vpn"

#     ``` bash title="Connect to VPN"
#     --8<-- "SSH-VPN-VNC/connect2vpn"
#     ```

for ifile in list:
  if '.py' in ifile:
    continue
  if '.md' in ifile:
    continue
  print(f"adding {ifile}")
  file.write(f"""
??? note "{ifile}"

    ``` sh title="{ifile}"
    --8<-- "{pwd}/{ifile}"
    ```
""")
  # file.write('- [' + str(l) + '](./config_files/' + str(l) +')\n')
file.close()