#!/bin/bash
# run with 'source modules_jet_intel.sh'

module purge
module load intel-oneapi-compilers/2022.2.1-zkofgc5\
 hdf5/1.12.2-intel-2021.7.1-w5sw2dq\
 netcdf-fortran/4.5.3-intel-2021.7.1-27ldrnt\
 netcdf-c/4.7.4-intel-2021.7.1-lnfs5zz\
 intel-oneapi-mpi/2021.7.1-intel-2021.7.1-pt3unoz
