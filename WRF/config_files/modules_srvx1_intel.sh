#!/bin/bash
# execute with 'source modules_srvx1_intel.sh'

module purge
module load intel-oneapi-compilers/2021.4.0 \
  intel-oneapi-mpi/2021.7.1-intel-2021.4.0 \
  hdf5/1.12.2-intel-2021.4.0 \
  netcdf-c/4.7.4-intel-2021.4.0 \
  netcdf-fortran/4.5.3-intel-2021.4.0

