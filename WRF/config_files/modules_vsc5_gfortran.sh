#!/bin/bash

spack unload --all
spack load netcdf-fortran@4.5.3%gcc@11.2.0

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:\
/gpfs/opt/sw/spack-0.17.1/opt/spack/linux-almalinux8-zen3/gcc-11.2.0/hdf5-1.10.7-xktnxpzgzprfgpldxnnkjsu7ostkg37b/lib:\
/gpfs/opt/sw/spack-0.17.1/opt/spack/linux-almalinux8-zen3/gcc-11.2.0/netcdf-c-4.8.1-jsfjwaz7qp52fjxfeg6mbhtt2lj3l573/lib:\
/gpfs/opt/sw/spack-0.17.1/opt/spack/linux-almalinux8-zen3/gcc-11.2.0/netcdf-fortran-4.5.3-t6jqlxq6bnz62g4kwezpffg6fzj4d6qg/lib
