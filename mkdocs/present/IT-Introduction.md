class: inverse
layout: true
name: Introduction to IT for new employees

---
class: title, center, middle

# Bare metal, shells and clouds ​- IT2Research​

![](./img/logo_imgw.svg)
![](../img/logo_fgga2.svg)


18.03.2024 - M. Blaschek - PhD Seminar @ IMGW

---

# What can you expect from this presentation?

- Asking Questions (anytime, please interrupt) - This presentation is for you!
--

- Hardware 
- Data and Storage
--

- Services
    - Teachinghub via Moodle
- Rules
--

- Updates
    - VSCode
--

---
# Bare metal @ Department


.left-column[
### AURORA
.right[<img src="../img/cpu.png" width="50px"><img src="../img/cpu.png" width="50px">]
Arsenal, **Development & Visual (VNC) Node**
- Storage (800 TB)
- Storage (11 TB SSD)

### JET
.right[<img src="../img/cpu.png" width="50px"><img src="../img/cpu.png" width="50px"><br><img src="../img/cluster-computing.png" width="100px">]
Arsenal, **Computing Cluster**
- 2x Login (JET01 (vnc),JET02 (hub))
- 17x Compute
- Storage (3.7 PB)
- SLURM
- JET2VSC connection
]

.right-column[
### SRVX1
.right[<img src="../img/cpu.png" width="50px"><img src="../img/cpu.png" width="50px"><img src="../img/cpu.png" width="50px"><img src="../img/cpu.png" width="50px">]
Arsenal, **Teaching Node**

### VSC
.right[<img src="../img/cpu.png" width="50px"><img src="../img/cpu.png" width="50px"><br><img src="../img/cluster-computing.png" width="100px"><br><img src="../img/gpu.png" width="100px">]
<img src="../img/logo_vsc.png" width="50px">
<br>Arsenal, **HPC Cluster**
- VSC4 5x Nodes
- **VSC5 11 Nodes**
- VSC5 1x GPU
- Shared HOME (200GB)
- Shared DATA (100TB)
- **# of files!!!**
- SLURM
- Project resources
]

---

# Important Things @UNIVIE

The ZID offers a lot of information on its services, most importantly your **u:account**.

Services:

- [u:account](https://zid.univie.ac.at/uaccount/) everything depends on it
- [vpn](https://zid.univie.ac.at/vpn/) access to only university wide things (our servers)
- [u:cloud](https://ucloud.univie.ac.at) University cloud
- [u:access](https://bibliothek.univie.ac.at/en/uaccess.html) library access for publications
- [gitlab/mattermost](https://gitlab.phaidra.org/imgw) code development / socializing
- [u:wiki](https://wiki.univie.ac.at/display/theomet) documentation of workflows, group communication, tutorials, intranet, ...
    * Lots of tutorials for specific problems (travel, expenses, new employees, printer, ...)
    * HPC / Computing information and guidelines
    * Please login once. More access can be granted afterwards.
- [wolke.img.univie.ac.at](https://wolke.img.univie.ac.at)
    * Landing Page of IT service @ IMGW
    * User Management [IPA](https://wolke.img.univie.ac.at/ipa/ui)

---
class: center, middle
# How to connect? -  HOME / UNI

![](../img/IMGW-connection-diagram.png)
![](../img/IMGW-connection-diagram2.png)

---

# Tools - How to connect?
Depending on your operating system (OS) you should find the right tool to connect. 

Options:

- Web browser - Jupyterhub running Jupyter Notebooks
    * Teaching Hub on [teaching.wolke](https://teaching.wolke.img.univie.ac.at)
    * Master Hub on [quell.wolke](https://quell.wolke.img.univie.ac.at)
    * Research Hub on [jupyter.wolke](https://jupyter.wolke.img.univie.ac.at)
- SSH (Windows: Putty, ...)
- SFTP (Windows: Mobaxterm, winscp, ...)
- VNC to JET01 or AURORA
    * install a vnc client on your OS
    * launch a vnc server, run: `userservices vnc` 
    * connect to VNC via a vnc viewer (tigervnc) `server:port`
    * more information on @gitlab @VNC

---

# Login and Passwords 

When you login the first time, you will need to change your password. **Please be advised that you should choose a secure one.** *A password manager is recommended.*

```bash
$ ssh [user]@login.img.univie.ac.at
$ ssh [user]@jet01.img.univie.ac.at
$ ssh [user]@jet02.img.univie.ac.at
# from home to jet directly
$ ssh -J [user]@login.img.univie.ac.at [user]@jet01.img.univie.ac.at
```

Each of these require a different password:

- IMGW Servers
- u:account
- eduroam (WLAN)
- VSC
- Gitlab / Mattermost

---

# Services @ Servers

All servers try to be up to date and will be updated regularly, the base OS is Red Hat OS 8/AlmaLinux 8. 

All servers handle software like this:
- environment modules, run: `modules av`
    * different compilers: gnu, intel
    * libraries: math, eccodes, netcdf ,...
    * different MPI
    * tools: teleport, ecaccess, ...
- userservices, run: `userservices`
    * **quota**
    * server-schedule
    * weather
    * containers
    * yopass
    * filetransfer
    * transfersh
- singularity containers, advanced topic.
    * building singularity containers for HPC applications
    
---

# Services @ Servers 2

Locations on Servers are very important. Where to store and find what?

| name        | Server | Path                           | Quota  | comment                      |
| ----------- | ------ | ------------------------------ | ------ | ---------------------------- |
| HOME        | aurora | `/srvfs/home/[user]`           | 100 GB | source code, daily backup    |
| HOME        | jet01  | `/jetfs/home/[user]`           | 100 GB | source code, daily backup    |
| ---         | ---    | ---                            | ---    | ---                          |
| SCRATCH     | aurora | `/srvfs/scratch/[user]`        | 1 TB   | data, no backup              |
| SCRATCH     | aurora | `/srvfs/tmp/[user]`            | 1 TB   | data, no backup              |
| SCRATCH     | jet01  | `/jetfs/scratch/[user]`        | no     | data, no backup              |
| ---         | ---    | ---                            | ---    | ---                          |
| DATA        | aurora | `/srvfs/data/[dataset]`        | no     | data, daily backup, constant |
| SHARED-DATA | aurora | `/srvfs/shared/[dataset]`      | no     | data, no backup, shared      |
| SHARED-DATA | jet01  | `/jetfs/shared-data/[dataset]` | no     | data, no backup, shared      |

All these paths can be remembered by running **`userpaths`**


---

# DATA Management

Science is about computing, but mostly about data and storing that data.

| name        | Server | Path                           |
| ----------- | ------ | ------------------------------ |
| DATA        | aurora | `/srvfs/data/[dataset]`        |
| SHARED-DATA | aurora | `/srvfs/shared/[dataset]`      |
| SHARED-DATA | jet01  | `/jetfs/shared-data/[dataset]` |


Please fill in the **Database** and follow the **Data Guidelines**, that can be found in these directories.

**Add a short decsription of your data and whom to contact for more information.**


---

# Support & Contacts

In case you need help, whom to contact?

- Servers > M. Blaschek
- VSC > M. Blaschek
- Gitlab > M. Blaschek
- Everything else > M. Ristic

Use Mattermost or send mail to [IT](mailto:it.img-wien@univie.ac.at) or [Gitlab Issues](https://gitlab.phaidra.org/imgw/computer-resources/-/issues)


---
class: center, title

# Questions

```
???????
?????
???
??
?
```
