class: inverse
layout: true

---

# Compiling code

**author: Jan Zabloudil**

![](https://vsc.ac.at//fileadmin/templates/vsc/logo_vsc.gif)

[VSC Gilab](https://gitlab.tuwien.ac.at/vsc-public/training/vsc-intro/-/blob/main/compiling/compiling.md?plain=0)

[open online](https://webdata.wolke.img.univie.ac.at/imgw/Presentation.html?file=compiling.md)

---

# Compiling code

## CPU architectures Skylake and Cascadelake:

These architectures are available in the following nodes/partitions:

* VSC-4 Login nodes l4[0-9]
* skylake_0096
* skylake_0384
* skylake_0768
* cascadelake_0384

You can choose between these compilers:

* Intel
* GNU

Usually on these architectures the Intel compiler delivers the best performance.

---

# Compiling code

## CPU architectures Zen2 and Zen3:

These architectures are available in the following nodes/partitions:

* VSC-5 Login nodes l5[0-6]
* zen3_0512
* zen3_1024
* zen3_2048
* gpu_a100_dual
* zen2_0256_a40x2

You can choose between these compilers:

* Intel
* GNU
* AOCC

Usually on these architectures the AOCC compiler delivers the best performance.

---

# Compiling code

## Skylake environment

Modules:

Intel compilers:

```shell
(skylake) [VSC-4]$ module load intel/19
(skylake) [VSC-4]$ module load compiler/latest
```

GNU compilers:

```shell
(skylake) [VSC-4]$ module load gcc/12.2.0-gcc-8.5.0-aal4zp2
(skylake) [VSC-4]$ module load gcc/9.1.0-gcc-8.5.0-mj7s6dg
```

---

# Compiling code

## Zen3 environment

Modules:

Intel compilers:

```shell
(zen3) [VSC-5]$ module load intel/19
(zen3) [VSC-5]$ module load compiler/latest
```

GNU compilers:

```shell
(zen3) [VSC-5]$ module load gcc/11.2.0-gcc-11.2.0-5i4t2bo
(zen3) [VSC-5]$ module load gcc/10.3.0-gcc-11.2.0-3k4sof7
```

AOCC compiler:

```shell
(zen3) [VSC-5]$ module load aocc/3.2.0-gcc-11.2.0-y53mdzy
```

---

# Compiling code

## Compiler names

Intel:

```shell
icc       # C compiler
icpc      # C++ compiler
ifort     # Fortran compiler
```

GNU:

```shell
gcc       # C compiler
g++/c++   # C++ compiler
gfortran  # Fortran compiler
```

AOCC:

```shell
clang     # C compiler
clang++   # C++ compiler
flang     # Fortran compiler
```

---

# Compiling code

## Examples: compiling serial code


* Compiler: Intel
* Language: C
* *~training/examples/15_compiling/hello.c*

```shell
(skylake/zen3) [VSC-4/5]$ module load compiler/latest
```

Compile without optimization:

```shell
(skylake/zen3) [VSC-4/5]$ icc -O0 hello.c -o hello_c
(skylake/zen3) [VSC-4/5]$ ./hello_c
Hello World
```

Compile with optimization:

```shell
(skylake/zen3) [VSC-4/5]$ icc -O3 -xHost hello.c -o hello_c
(skylake/zen3) [VSC-4/5]$ ./hello_c
Hello World
```

---

# Compiling code

## Examples: compiling serial code


* Compiler: GNU
* Language: C
* *~training/examples/15_compiling/hello.c*

```shell
(skylake) [VSC-4]$ module load gcc/12.2.0-gcc-8.5.0-aal4zp2
(zen3) [VSC-5]$ module load gcc/11.2.0-gcc-11.2.0-5i4t2bo
```

Compile without optimization:

```shell
(skylake/zen3) [VSC-4/5]$ gcc -O0 hello.c -o hello_c
(skylake/zen3) [VSC-4/5]$ ./hello_c
Hello World
```

---

# Compiling code

## Examples: compiling serial code


* Compiler: GNU
* Language: C
* *~training/examples/15_compiling/hello.c*


Compile with optimization:

```shell
(skylake) [VSC-4]$ gcc -O2 -march=skylake hello.c -o hello_c
(zen3) [VSC-5]$ gcc -O2 -march=znver3 hello.c -o hello_c
```

```shell
(skylake/zen3) [VSC-4/5]$ ./hello_c
Hello World
```

---

# Compiling code

## Examples: compiling serial code


* Compiler: AOCC
* Language: C
* *~training/examples/15_compiling/hello.c*

```shell
(zen3) [VSC-5]$ module load aocc/3.2.0-gcc-11.2.0-y53mdzy
```

Compile without optimization:

```shell
(zen3) [VSC-5]$ clang -O0 hello.c -o hello_c
(zen3) [VSC-5]$ ./hello_c
Hello World
```

---

# Compiling code

## Examples: compiling serial code


* Compiler: AOCC
* Language: C
* *~training/examples/15_compiling/hello.c*

Compile with optimization:

```shell
(zen3) [VSC-5]$ clang -O3 -march=znver3 hello.c -o hello_c
```

```shell
(zen3) [VSC-5]$ ./hello_c
Hello World
```

---

# Compiling code 

## Examples: compiling OPENMP code:

* Compiler: Intel
* Language: C
* *~training/examples/15_compiling/hello-openmp.c*

```shell
(skylake/zen3) [VSC-4/5]$ icc -qopenmp hello_openmp.c -o hello_openmp_c
(skylake/zen3) [VSC-4/5]$ export OMP_NUM_THREADS=2
(skylake/zen3) [VSC-4/5]$ ./hello_openmp_c
Hello World... from thread = 0
Hello World... from thread = 1
```

---

# Compiling code

## Examples: compiling OPENMP code:

* Compiler: GNU
* Language: C
* *~training/examples/15_compiling/hello_openmp.c*

```shell
(skylake/zen3) [VSC-4/5]$ gcc -fopenmp hello_openmp.c -o hello_openmp_c
(skylake/zen3) [VSC-4/5]$ export OMP_NUM_THREADS=2
(skylake/zen3) [VSC-4/5]$ ./hello_openmp_c
Hello World... from thread = 0
Hello World... from thread = 1
```


---

# Compiling code

## Examples: compiling OPENMP code:

* Compiler: AOCC
* Language: C
* *~training/examples/15_compiling/hello_openmp.c*

```shell
(skylake/zen3) [VSC-4/5]$ clang -fopenmp hello_openmp.c hello_openmp_c
(skylake/zen3) [VSC-4/5]$ export OMP_NUM_THREADS=2
(skylake/zen3) [VSC-4/5]$ ./hello_openmp_c
Hello World... from thread = 0
Hello World... from thread = 1
```

---

# Compiling code 

## Compiling MPI code:

* Use compiler wrappers
* Various MPI implementations available:
  * Intel MPI
  * OpenMPI

---

# Compiling code

## Compiling MPI code: Intel MPI

Compiler wrappers for Intel compiler:

```shell
mpiicc              # C
mpiicpc             # C++
mpiifort            # Fortran
```

Compiler wrappers for GNU compiler:

```shell
mpicc/mpigcc        # C
mpicxx/mpigxx       # C++
mpif90/mpif77/mpifc # Fortran
```


---

# Compiling code

## Compiling MPI code: Intel MPI

```shell
(skylake) [VSC-4]$ module purge
(skylake) [VSC-4]$ module load compiler/latest intel-oneapi-mpi/2021.6.0-intel-2021.5.0-wpt4y32
(skylake) [VSC-4]$ mpiicc -show
(skylake) [VSC-4]$ module purge
(skylake) [VSC-4]$ module load gcc/12.2.0-gcc-8.5.0-aal4zp2 intel-oneapi-mpi/2021.6.0-intel-2021.5.0-wpt4y32
(skylake) [VSC-4]$ mpicc -show
```


---

# Compiling code

## Compiling MPI code: OpenMPI

Compiler specific installations, e.g.: <!-- .element: class="fragment highlight-current-red" -->

```shell
(skylake) [VSC-4]$ module load openmpi/4.1.4-intel-19.1.3.304-6xqnw75
(skylake) [VSC-4]$ module load openmpi/4.1.4-gcc-12.2.0-owirnm6
```

```shell
(zen3) [VSC-5]$ module load openmpi/4.1.4-gcc-11.2.0-ub765vm
(zen3) [VSC-5]$ module load openmpi/4.1.4-aocc-3.2.0-7abidzt
```


Compiler wrappers: <!-- .element: class="fragment highlight-current-red" -->

```shell
mpicc                    # C
mpic++/mpiCC/mpicxx      # C++
mpif77/mpif90/mpifort    # Fortran
```

---

# Compiling code

## Examples: compiling MPI code

* Compiler: Intel
* MPI: Intel
* Language: C
* *~training/examples/15_compiling/hello_mpi.c*

```shell
(skylake) [VSC-4]$ module purge
(skylake) [VSC-4]$ module load compiler/latest intel-oneapi-mpi/2021.6.0-intel-2021.5.0-wpt4y32
```

Compile without optimization: <!-- .element: class="fragment highlight-current-red" -->

```shell
(skylake) [VSC-4]$ mpiicc -O0 hello-mpi.c -o hello-mpi_c
(skylake) [VSC-4]$ mpirun -np 2 ./hello-mpi_c
Hello world from processor l44, rank 1 out of 2 processors
Hello world from processor l44, rank 0 out of 2 processors
```

---

# Compiling code

## Examples: compiling MPI code

* Compiler: Intel
* MPI: Intel
* Language: C
* *~training/examples/15_compiling/hello_mpi.c*

```shell
(skylake) [VSC-4]$ module purge
(skylake) [VSC-4]$ module load compiler/latest intel-oneapi-mpi/2021.6.0-intel-2021.5.0-wpt4y32
```

Compile with optimization: <!-- .element: class="fragment highlight-current-red" -->

```shell
(skylake) [VSC-4]$ mpiicc -O3 -xHost hello-mpi.c -o hello-mpi_c
(skylake) [VSC-4]$ mpirun -np 2 ./hello-mpi_c
Hello world from processor l44, rank 1 out of 2 processors
Hello world from processor l44, rank 0 out of 2 processors
```

---

# Compiling code

## Examples: compiling MPI code

* Compiler: GNU
* MPI: Intel
* Language: C
* *~training/examples/15_compiling/hello_mpi.c*

```shell
(skylake) [VSC-4]$ module purge
(skylake) [VSC-4]$ module load gcc/12.2.0-gcc-8.5.0-aal4zp2 intel-oneapi-mpi/2021.6.0-intel-2021.5.0-wpt4y32
```

Compile without optimization: <!-- .element: class="fragment highlight-current-red" -->

```shell
(skylake) [VSC-4]$ mpicc -O0 hello-mpi.c -o hello-mpi_c
(skylake) [VSC-4]$ mpirun -np 2 ./hello-mpi_c
Hello world from processor l44, rank 1 out of 2 processors
Hello world from processor l44, rank 0 out of 2 processors
```

---

# Compiling code

## Examples: compiling MPI code

* Compiler: GNU
* MPI: Intel
* Language: C
* *~training/examples/15_compiling/hello_mpi.c*

```shell
(skylake) [VSC-4]$ module purge
(skylake) [VSC-4]$ module load gcc/12.2.0-gcc-8.5.0-aal4zp2 intel-oneapi-mpi/2021.6.0-intel-2021.5.0-wpt4y32
```

Compile with optimization: <!-- .element: class="fragment highlight-current-red" -->

```shell
(skylake) [VSC-4]$ mpicc -O2 -march=skylake hello-mpi.c -o hello-mpi_c
(skylake) [VSC-4]$ mpirun -np 2 ./hello-mpi_c
Hello world from processor l44, rank 1 out of 2 processors
Hello world from processor l44, rank 0 out of 2 processors
```

---

# Compiling code

## Examples: compiling MPI code

* Compiler: AOCC
* MPI: Openmpi
* Language: C
* *~training/examples/15_compiling/hello_mpi.c*

```shell
(zen3) [VSC-5]$ module purge
(zen3) [VSC-5]$ module load aocc/3.2.0-gcc-11.2.0-y53mdzyr openmpi/4.1.4-aocc-3.2.0-7abidzt
```

Compile without optimization: <!-- .element: class="fragment highlight-current-red" -->

```shell
(zen3) [VSC-5]$ mpicc -O0 hello-mpi.c -o hello-mpi_c
(zen3) [VSC-5]$ mpirun -np 2 ./hello-mpi_c
Hello world from processor l54.vsc.xcat, rank 1 out of 2 processors
Hello world from processor l54.vsc.xcat, rank 0 out of 2 processors
```

---

# Compiling code

## Examples: compiling MPI code

* Compiler: AOCC
* MPI: Openmpi
* Language: C
* *~training/examples/15_compiling/hello_mpi.c*

```shell
(zen3) [VSC-5]$ module purge
(zen3) [VSC-5]$ module load aocc/3.2.0-gcc-11.2.0-y53mdzyr openmpi/4.1.4-aocc-3.2.0-7abidzt
```

Compile with optimization: <!-- .element: class="fragment highlight-current-red" -->

```shell
(zen3) [VSC-5]$ mpicc -O2 -march=znver3 hello-mpi.c -o hello-mpi_c
(zen3) [VSC-5]$ mpirun -np 2 ./hello-mpi_c
Hello world from processor l54.vsc.xcat, rank 1 out of 2 processors
Hello world from processor l54.vsc.xcat, rank 0 out of 2 processors
```
---

