# Documentation static site generator & deployment tool
mkdocs>=1.4.2
# Theme
mkdocs-material>=8.5.2
mkdocs-material-extensions>=1.1
# Plugins
mkdocs-awesome-pages-plugin
mkdocs-git-revision-date-localized-plugin
mkdocs-jupyter
mkdocs-same-dir
mkdocs-exclude
mkdocs-graphviz